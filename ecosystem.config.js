const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });

module.exports = {
  apps: [{
    name: 'Twitch player',
    script: 'bin/www',
    env: {
      NODE_ENV: 'production'
    },
    error_file : "./logs/err.log",
    out_file : "./logs/out.log",
    env_production: {
      NODE_ENV: 'production'
    },
    instances: 1,
    exec_mode: 'cluster'
  },
],

  deploy: {
    production: {
      user: 'node',
      host: '212.83.163.1',
      ref: 'origin/master',
      repo: 'git@github.com:repo.git',
      path: '/var/www/production',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
}
