function makeIframeCode(platform, id, type = '') {
    return `<iframe src="https://player.uplify.app/p?id=${id}&platform=${platform}&twitch_type=${type}&section_id=3871321&w=320&h=180&autoplay=1" frameborder="0" allowfullscreen="true" scrolling="no" height="180" width="320"></iframe>`
}

function makePlayer() {
    return `<div id="container" style=""> </div>`;
}

function makeJsCode(platform, id, type = '', mode = 'normal') {
    let w = 360, h = 210;
    if (mode === 'fixed' || mode === 'mixed') {
      w = 400;
      h = 225;
    }
    return `<div id="container" style="width: 100%; height: 100%;"></div>
<script type="text/javascript" src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
<!-- skip this line if Twitch API is already embeded on the page -->
<script src= "https://player.twitch.tv/js/embed/v1.js"></script>
<script type="text/javascript" src="https://player.uplify.app/scripts/dist/application.js"></script>
<script>
  window.onload = function() {
    const application = new UPApp({
      platform: '${platform}',
      twitchType: '${type}',
      platformId: '${id}',
      sectionId: 3871321,
      width: 640,
      height: 320,
      widthFloated: ${w},
      heigthFloated: ${h},
      interval: 600,
      mode: '${mode}',
      title: 'ANY TITLE TO ATTRACT USERS'
    });
  };
</script>`;
  }

window.onload = function() {
  const content1 = document.getElementById('container-1');
  const content2 = document.getElementById('container-2');
  const content3 = document.getElementById('container-3');
  let app;
  
  function toggleCurrentDemo(current) {
      if (app && app.ads_) {
        app.ads_.clear();
      }
      
      const containers = [content1, content2, content3];
      for (let i = 0; i < containers.length; i++) {
          if (current == i + 1) {
              containers[i].style.display = 'block';
          } else {
              containers[i].style.display = 'none';
              containers[i].innerHTML = '';
          }
      }
  }
  
  document.getElementById('streamer__name__btn').addEventListener('click', function(e) {
      const streamerId = document.getElementById('streamer__name').value;
      content1.innerHTML = '';
      // content1.innerHTML = makeIframeCode('twitch', streamerId);
      content1.innerHTML = makePlayer();
      document.getElementById('videoplayer-1-code').value = makeIframeCode('twitch', streamerId);
      document.getElementById('videoplayer-1-js').value = makeJsCode('twitch', streamerId, '', 'fixed');
      toggleCurrentDemo(1);
      app = new UPApp({
        platform: 'twitch',
        twitchType: '',
        platformId: streamerId,
        sectionId: '3585837',
        width: 640,
        height: 320,
        widthFloated: 400,
        heigthFloated: 225,
        interval: 70,
        autoplay: false, 
        mode: 'fixed',
        title: 'ANY TITLE TO ATTRACT USERS'
      });
  });

  document.getElementById('twitch__id__btn').addEventListener('click', function(e) {
      const videoId = document.getElementById('twitch__id').value;
      content2.innerHTML = '';
      // content2.innerHTML = makeIframeCode('twitch', videoId, 'video');
      content2.innerHTML = makePlayer();
      document.getElementById('videoplayer-2-code').value = makeIframeCode('twitch', videoId, 'video');
      document.getElementById('videoplayer-2-js').value = makeJsCode('twitch', videoId, 'video', 'mixed');
      toggleCurrentDemo(2);
      app = new UPApp({
        platform: 'twitch',
        twitchType: 'video',
        platformId: videoId,
        sectionId: '3585837',
        width: 640,
        height: 320,
        widthFloated: 400,
        heigthFloated: 225,
        interval: 70,
        autoplay: false, 
        mode: 'mixed',
        title: 'ANY TITLE TO ATTRACT USERS'
      });
  });

  document.getElementById('youtube__id__btn').addEventListener('click', function(e) {
      const videoId = document.getElementById('youtube__id').value;
      content3.innerHTML = '';
    //   content3.innerHTML = makeIframeCode('youtube', videoId);
    content3.innerHTML = makePlayer();
      document.getElementById('videoplayer-3-code').value = makeIframeCode('youtube', videoId);
      document.getElementById('videoplayer-3-js').value = makeJsCode('youtube', videoId, 'normal');
      toggleCurrentDemo(3);
      app = new UPApp({
        platform: 'youtube',
        platformId: videoId,
        sectionId: '3585837',
        width: 640,
        height: 320,
        interval: 70,
        autoplay: false, 
        mode: 'normal'
      });
  });


//   document.getElementById('twitch__id__btn').click();

};


