// start utils
var NODE_TYPE_ELEMENT = 1;

function extend(obj) {
    var arg, i, k;
    for (i = 1; i < arguments.length; i++) {
        arg = arguments[i];
        for (k in arg) {
            if (arg.hasOwnProperty(k)) {
                if (isObject(obj[k]) && !isNull(obj[k]) && isObject(arg[k])) {
                    obj[k] = extend({}, obj[k], arg[k]);
                } else {
                    obj[k] = arg[k];
                }
            }
        }
    }
    return obj;
}

function isString(str) {
    return typeof str === 'string';
}

function isNotEmptyString(str) {
    return isString(str) && str.length !== 0;
}

function isNull(o) {
    return o === null;
}

function isObject(obj) {
    return typeof obj === 'object';
}

function isFunction(str) {
    return typeof str === 'function';
}

function forEach(obj, iterator, context) {
    var key, length;
    if (obj) {
        if (isFunction(obj)) {
            for (key in obj) {
                // Need to check if hasOwnProperty exists,
                // as on IE8 the result of querySelectorAll is an object without a hasOwnProperty function
                if (key !== 'prototype' && key !== 'length' && key !== 'name' && (!obj.hasOwnProperty || obj.hasOwnProperty(key))) {
                    iterator.call(context, obj[key], key, obj);
                }
            }
        } else if (isArray(obj)) {
            var isPrimitive = typeof obj !== 'object';
            for (key = 0, length = obj.length; key < length; key++) {
                if (isPrimitive || key in obj) {
                    iterator.call(context, obj[key], key, obj);
                }
            }
        } else if (obj.forEach && obj.forEach !== forEach) {
            obj.forEach(iterator, context, obj);
        } else {
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    iterator.call(context, obj[key], key, obj);
                }
            }
        }
    }
    return obj;
}

function isArray(array) {
    return Object.prototype.toString.call(array) === '[object Array]';
}

function isWindow(obj) {
    return isObject(obj) && obj.window === obj;
}

function isUndefined(o) {
    return o === undefined;
}

function isArrayLike(obj) {
    if (obj === null || isWindow(obj) || isFunction(obj) || isUndefined(obj)) {
        return false;
    }

    var length = obj.length;

    if (obj.nodeType === NODE_TYPE_ELEMENT && length) {
        return true;
    }

    return isString(obj) || isArray(obj) || length === 0 ||
        typeof length === 'number' && length > 0 && (length - 1) in obj;
}

function arrayLikeObjToArray(args) {
    return Array.prototype.slice.call(args);
}

// end utils

// start mini-dom

var dom = {};

dom.addClass = function (el, cssClass) {
    var classes;

    if (isNotEmptyString(cssClass)) {
        if (el.classList) {
            return el.classList.add(cssClass);
        }

        classes = isString(el.getAttribute('class')) ? el.getAttribute('class').split(/\s+/) : [];
        if (isString(cssClass) && isNotEmptyString(cssClass.replace(/\s+/, ''))) {
            classes.push(cssClass);
            el.setAttribute('class', classes.join(' '));
        }
    }
};

dom.removeClass = function (el, cssClass) {
    var classes;

    if (isNotEmptyString(cssClass)) {
        if (el.classList) {
            return el.classList.remove(cssClass);
        }

        classes = isString(el.getAttribute('class')) ? el.getAttribute('class').split(/\s+/) : [];
        var newClasses = [];
        var i, len;
        if (isString(cssClass) && isNotEmptyString(cssClass.replace(/\s+/, ''))) {

            for (i = 0, len = classes.length; i < len; i += 1) {
                if (cssClass !== classes[i]) {
                    newClasses.push(classes[i]);
                }
            }
            el.setAttribute('class', newClasses.join(' '));
        }
    }
};


dom.addEventListener = function addEventListener(el, type, handler) {
    if (isArray(el)) {
        forEach(el, function (e) {
            dom.addEventListener(e, type, handler);
        });
        return;
    }

    if (isArray(type)) {
        forEach(type, function (t) {
            dom.addEventListener(el, t, handler);
        });
        return;
    }

    if (el.addEventListener) {
        el.addEventListener(type, handler, false);
    } else if (el.attachEvent) {
        // WARNING!!! this is a very naive implementation !
        // the event object that should be passed to the handler
        // would not be there for IE8
        // we should use "window.event" and then "event.srcElement"
        // instead of "event.target"
        el.attachEvent("on" + type, handler);
    }
};

dom.removeEventListener = function removeEventListener(el, type, handler) {
    if (isArray(el)) {
        forEach(el, function (e) {
            dom.removeEventListener(e, type, handler);
        });
        return;
    }

    if (isArray(type)) {
        forEach(type, function (t) {
            dom.removeEventListener(el, t, handler);
        });
        return;
    }

    if (el.removeEventListener) {
        el.removeEventListener(type, handler, false);
    } else if (el.detachEvent) {
        el.detachEvent("on" + type, handler);
    } else {
        el["on" + type] = null;
    }
};

dom.onReady = (function () {
    var readyHandlers = [];
    var readyFired = false;

    // if document already ready to go, schedule the ready function to run
    // IE only safe when readyState is "complete", others safe when readyState is "interactive"
    if (document.readyState === "complete" || (!document.attachEvent && document.readyState === "interactive")) {
        setTimeout(ready, 0);
    } else {
        // otherwise if we don't have event handlers installed, install them
        if (document.addEventListener) {
            // first choice is DOMContentLoaded event
            document.addEventListener("DOMContentLoaded", ready, false);
            // backup is window load event
            window.addEventListener("load", ready, false);
        } else {
            // must be IE
            document.attachEvent("onreadystatechange", readyStateChange);
            window.attachEvent("onload", ready);
        }
    }

    return function documentOnReady(handler, context) {
        context = context || window;

        if (isFunction(handler)) {
            if (readyFired) {
                setTimeout(function () {
                    handler.bind(context);
                }, 0);
            } else {
                readyHandlers.push(handler.bind(context));
            }
        }
    };

    /*** Local functions ****/
    function ready() {
        if (!readyFired) {
            readyFired = true;
            forEach(readyHandlers, function (handler) {
                handler();
            });
            readyHandlers = [];
        }
    }

    function readyStateChange() {
        if (document.readyState === "complete") {
            ready();
        }
    }
})();

dom.prependChild = function prependChild(parent, child) {
    if (child.parentNode) {
        child.parentNode.removeChild(child);
    }
    return parent.insertBefore(child, parent.firstChild);
};

dom.remove = function removeNode(node) {
    if (node && node.parentNode) {
        node.parentNode.removeChild(node);
    }
};

// end mini-dom

// start messages 

var MESSAGE_DURATION = 3500;
var MSG_TYPE = {
    SUCCESS: 'msg-success',
    ERROR: 'msg-error'
};
var timeoutId = null;

var messageContainer = document.createElement('div');

dom.onReady(function () {
    document.body.appendChild(messageContainer);
});

dom.addClass(messageContainer, 'messages');

function showMessage(type, msg) {
    if (timeoutId) {
        clearTimeout(timeoutId);
    }
    dom.addClass(messageContainer, MSG_TYPE[type]);
    messageContainer.innerHTML = msg;

    timeoutId = setTimeout(resetMessageContainer, MESSAGE_DURATION);
}

function resetMessageContainer() {
    forEach(MSG_TYPE, function (className) {
        dom.removeClass(messageContainer, className);
    });
    messageContainer.innerHTML = '';
    timeoutId = null;
}

function showSuccessMessage(msg) {
    showMessage('SUCCESS', msg);
}

function showErrorMessage(msg) {
    showMessage('ERROR', msg);
}

// end messages

// ads-setup plagin


function molVastSetup(opts) {
    var player = this;
    var options = extend({}, this.options_, opts);

    var pluginSettings = {
        playAdAlways: true,
        adCancelTimeout: options.adCancelTimeout || 3000,
        adsEnabled: !!options.adsEnabled,
        vpaidFlashLoaderPath: './scripts/VPAIDFlash.swf'
    };

    if (options.adTagUrl) {
        pluginSettings.adTagUrl = options.adTagUrl;
    }

    if (options.adTagXML) {
        pluginSettings.adTagXML = options.adTagXML;
    }

    var vastAd = player.vastClient(pluginSettings);

    player.on('reset', function () {
        if (player.options().plugins['ads-setup'].adsEnabled) {
            vastAd.enable();
        } else {
            vastAd.disable();
        }
    });

    player.on('vast.aderror', function (evt) {
        var error = evt.error;

        if (error && error.message) {
            // messages.error(error.message);
            console.log('ERROR', error.message);
        }
    });
};

// ads-setup end

// window.onload = function () {

videojs.plugin('ads-setup', molVastSetup);

dom.onReady(function () {
    var vastForm = document.querySelector('form#vast-vpaid-form');

    axios.get('https://match.ads.betweendigital.com/vpaid_prod?s3585837=&maxd=300&mind=1&w=640&h=480')
        .then((res) => {
            initForm(vastForm, res.data)
        }).catch((error) => {
            console.log('ERROR', error);
        });
    initForm(vastForm);

    /*** Local functions ***/
    function initForm(formEl, xml) {
        var tagTypeEl = formEl.querySelector('input.tag-type-radio');
        var xmlTypeEl = formEl.querySelector('input.xml-type-radio');
        var customTypeEl = formEl.querySelector('input.custom-type-radio');
        var updateBtn = formEl.querySelector('.button.button-primary');
        var pauseBtn = formEl.querySelector('.pause');
        var resumeBtn = formEl.querySelector('.resume');
        var tagEl = formEl.querySelector('input.tag-el');
        var xmlEl = formEl.querySelector('select.xml-el');
        var customEl = formEl.querySelector('textarea.custom-el');
        var videoContainer = formEl.querySelector('div.vjs-video-container');
        var player;

        updateVisibility();
        dom.addEventListener(tagTypeEl, 'change', updateVisibility);
        dom.addEventListener(xmlTypeEl, 'change', updateVisibility);
        dom.addEventListener(customTypeEl, 'change', updateVisibility);
        dom.addEventListener(updateBtn, 'click', function () {
            updateDemo();
            // messages.success("Demo updated!!!");
            console.log("Demo updated!!!");
        });

        if (pauseBtn && resumeBtn) {
            dom.addEventListener(pauseBtn, 'click', function () {
                pauseAd();
                // messages.success("ad paused");
                console.log("ad paused");
            });

            dom.addEventListener(resumeBtn, 'click', function () {
                resumeAd();
                // messages.success("ad resumed");
                console.log('ad resumed');
            });

        }

        updateDemo();

        /*** Local functions ***/
        function updateVisibility() {
            dom.removeClass(formEl, 'TAG');
            dom.removeClass(formEl, 'XML');
            dom.removeClass(formEl, 'CUSTOM');
            dom.addClass(formEl, activeMode());
        }

        function pauseAd() {
            if (player) {
                player.vast.adUnit.pauseAd();
                showResumeBtn();
            }
        }

        function resumeAd() {
            if (player) {
                player.vast.adUnit.resumeAd();
                showPauseBtn();
            }
        }

        function showResumeBtn() {
            pauseBtn.style.display = 'none';
            resumeBtn.style.display = 'inline-block';
        }

        function showPauseBtn() {
            pauseBtn.style.display = 'inline-block';
            resumeBtn.style.display = 'none';
        }

        function updateDemo() {
            createVideoEl(videoContainer, function (videoEl) {
                var mode = 'XML'// activeMode();
                var adPluginOpts = {
                    "plugins": {
                        "ads-setup": {
                            "adCancelTimeout": 20000, // Wait for ten seconds before canceling the ad.
                            "adsEnabled": true
                        }
                    }
                };

                if (mode === 'TAG') {
                    adPluginOpts.plugins["ads-setup"].adTagUrl = tagEl.value;
                } else if (mode === 'XML') {
                    adPluginOpts.plugins["ads-setup"].adTagUrl = 'https://match.ads.betweendigital.com/vpaid_prod?s3585837=&maxd=300&mind=1&w=640&h=480'; // xml; // xmlEl.value;
                } else {
                    adPluginOpts.plugins["ads-setup"].adTagXML = function (done) {
                        //The setTimeout is to simulate asynchrony
                        setTimeout(function () {
                            done(null, xml);
                        }, 0);
                    };
                }
                player = videojs(videoEl, adPluginOpts);

                //We hide the pause and resume btns every time we update
                if (pauseBtn) {
                    pauseBtn.style.display = 'none';
                    resumeBtn.style.display = 'none';
                }


                if (player) {
                    player.on('vast.adStart', function () {
                        showPauseBtn();
                        player.on('play', showPauseBtn);
                        player.on('pause', showResumeBtn);
                        player.one('vast.adEnd', function () {
                            pauseBtn.style.display = 'none';
                            resumeBtn.style.display = 'none';

                            player.off('play', showPauseBtn);
                            player.off('pause', showResumeBtn);
                        });
                    });
                }
            });
        }

        function activeMode() {
            if (tagTypeEl.checked) {
                return 'TAG';
            }

            if (xmlTypeEl.checked) {
                return 'XML';
            }

            return 'CUSTOM';
        }

        function createVideoEl(container, cb) {
            var videoTag = '<video class="video-js vjs-default-skin" controls preload="auto" poster="http://vjs.zencdn.net/v/oceans.png" >' +
                '<source src="http://vjs.zencdn.net/v/oceans.mp4" type="video/mp4"/>' +
                '<source src="http://vjs.zencdn.net/v/oceans.webm" type="video/webm"/>' +
                '<source src="http://vjs.zencdn.net/v/oceans.ogv" type="video/ogg"/>' +
                '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that ' +
                '<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>' +
                '</p>' +
                '</video>';
            container.innerHTML = videoTag;

            //We do this asynchronously to give time for the dom to be updated
            setTimeout(function () {
                var videoEl = container.querySelector('.video-js');
                cb(videoEl);
            }, 0);
        }
    }
});
// };