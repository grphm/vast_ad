// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"../../node_modules/axios/lib/helpers/bind.js":[function(require,module,exports) {
'use strict';

module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};

},{}],"../../node_modules/axios/lib/utils.js":[function(require,module,exports) {
'use strict';

var bind = require('./helpers/bind');

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is a Buffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Buffer, otherwise false
 */
function isBuffer(val) {
  return val !== null && !isUndefined(val) && val.constructor !== null && !isUndefined(val.constructor)
    && typeof val.constructor.isBuffer === 'function' && val.constructor.isBuffer(val);
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 * nativescript
 *  navigator.product -> 'NativeScript' or 'NS'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' ||
                                           navigator.product === 'NativeScript' ||
                                           navigator.product === 'NS')) {
    return false;
  }
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Function equal to merge with the difference being that no reference
 * to original objects is kept.
 *
 * @see merge
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function deepMerge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = deepMerge(result[key], val);
    } else if (typeof val === 'object') {
      result[key] = deepMerge({}, val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  deepMerge: deepMerge,
  extend: extend,
  trim: trim
};

},{"./helpers/bind":"../../node_modules/axios/lib/helpers/bind.js"}],"../../node_modules/axios/lib/helpers/buildURL.js":[function(require,module,exports) {
'use strict';

var utils = require('./../utils');

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    var hashmarkIndex = url.indexOf('#');
    if (hashmarkIndex !== -1) {
      url = url.slice(0, hashmarkIndex);
    }

    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};

},{"./../utils":"../../node_modules/axios/lib/utils.js"}],"../../node_modules/axios/lib/core/InterceptorManager.js":[function(require,module,exports) {
'use strict';

var utils = require('./../utils');

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;

},{"./../utils":"../../node_modules/axios/lib/utils.js"}],"../../node_modules/axios/lib/core/transformData.js":[function(require,module,exports) {
'use strict';

var utils = require('./../utils');

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};

},{"./../utils":"../../node_modules/axios/lib/utils.js"}],"../../node_modules/axios/lib/cancel/isCancel.js":[function(require,module,exports) {
'use strict';

module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};

},{}],"../../node_modules/axios/lib/helpers/normalizeHeaderName.js":[function(require,module,exports) {
'use strict';

var utils = require('../utils');

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};

},{"../utils":"../../node_modules/axios/lib/utils.js"}],"../../node_modules/axios/lib/core/enhanceError.js":[function(require,module,exports) {
'use strict';

/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }

  error.request = request;
  error.response = response;
  error.isAxiosError = true;

  error.toJSON = function() {
    return {
      // Standard
      message: this.message,
      name: this.name,
      // Microsoft
      description: this.description,
      number: this.number,
      // Mozilla
      fileName: this.fileName,
      lineNumber: this.lineNumber,
      columnNumber: this.columnNumber,
      stack: this.stack,
      // Axios
      config: this.config,
      code: this.code
    };
  };
  return error;
};

},{}],"../../node_modules/axios/lib/core/createError.js":[function(require,module,exports) {
'use strict';

var enhanceError = require('./enhanceError');

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};

},{"./enhanceError":"../../node_modules/axios/lib/core/enhanceError.js"}],"../../node_modules/axios/lib/core/settle.js":[function(require,module,exports) {
'use strict';

var createError = require('./createError');

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  if (!validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response.request,
      response
    ));
  }
};

},{"./createError":"../../node_modules/axios/lib/core/createError.js"}],"../../node_modules/axios/lib/helpers/isAbsoluteURL.js":[function(require,module,exports) {
'use strict';

/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};

},{}],"../../node_modules/axios/lib/helpers/combineURLs.js":[function(require,module,exports) {
'use strict';

/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
};

},{}],"../../node_modules/axios/lib/core/buildFullPath.js":[function(require,module,exports) {
'use strict';

var isAbsoluteURL = require('../helpers/isAbsoluteURL');
var combineURLs = require('../helpers/combineURLs');

/**
 * Creates a new URL by combining the baseURL with the requestedURL,
 * only when the requestedURL is not already an absolute URL.
 * If the requestURL is absolute, this function returns the requestedURL untouched.
 *
 * @param {string} baseURL The base URL
 * @param {string} requestedURL Absolute or relative URL to combine
 * @returns {string} The combined full path
 */
module.exports = function buildFullPath(baseURL, requestedURL) {
  if (baseURL && !isAbsoluteURL(requestedURL)) {
    return combineURLs(baseURL, requestedURL);
  }
  return requestedURL;
};

},{"../helpers/isAbsoluteURL":"../../node_modules/axios/lib/helpers/isAbsoluteURL.js","../helpers/combineURLs":"../../node_modules/axios/lib/helpers/combineURLs.js"}],"../../node_modules/axios/lib/helpers/parseHeaders.js":[function(require,module,exports) {
'use strict';

var utils = require('./../utils');

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = [
  'age', 'authorization', 'content-length', 'content-type', 'etag',
  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
  'referer', 'retry-after', 'user-agent'
];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};

},{"./../utils":"../../node_modules/axios/lib/utils.js"}],"../../node_modules/axios/lib/helpers/isURLSameOrigin.js":[function(require,module,exports) {
'use strict';

var utils = require('./../utils');

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
    (function standardBrowserEnv() {
      var msie = /(msie|trident)/i.test(navigator.userAgent);
      var urlParsingNode = document.createElement('a');
      var originURL;

      /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
      function resolveURL(url) {
        var href = url;

        if (msie) {
        // IE needs attribute set twice to normalize properties
          urlParsingNode.setAttribute('href', href);
          href = urlParsingNode.href;
        }

        urlParsingNode.setAttribute('href', href);

        // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
        return {
          href: urlParsingNode.href,
          protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
          host: urlParsingNode.host,
          search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
          hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
          hostname: urlParsingNode.hostname,
          port: urlParsingNode.port,
          pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
            urlParsingNode.pathname :
            '/' + urlParsingNode.pathname
        };
      }

      originURL = resolveURL(window.location.href);

      /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
      return function isURLSameOrigin(requestURL) {
        var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
        return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
      };
    })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return function isURLSameOrigin() {
        return true;
      };
    })()
);

},{"./../utils":"../../node_modules/axios/lib/utils.js"}],"../../node_modules/axios/lib/helpers/cookies.js":[function(require,module,exports) {
'use strict';

var utils = require('./../utils');

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
    (function standardBrowserEnv() {
      return {
        write: function write(name, value, expires, path, domain, secure) {
          var cookie = [];
          cookie.push(name + '=' + encodeURIComponent(value));

          if (utils.isNumber(expires)) {
            cookie.push('expires=' + new Date(expires).toGMTString());
          }

          if (utils.isString(path)) {
            cookie.push('path=' + path);
          }

          if (utils.isString(domain)) {
            cookie.push('domain=' + domain);
          }

          if (secure === true) {
            cookie.push('secure');
          }

          document.cookie = cookie.join('; ');
        },

        read: function read(name) {
          var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
          return (match ? decodeURIComponent(match[3]) : null);
        },

        remove: function remove(name) {
          this.write(name, '', Date.now() - 86400000);
        }
      };
    })() :

  // Non standard browser env (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return {
        write: function write() {},
        read: function read() { return null; },
        remove: function remove() {}
      };
    })()
);

},{"./../utils":"../../node_modules/axios/lib/utils.js"}],"../../node_modules/axios/lib/adapters/xhr.js":[function(require,module,exports) {
'use strict';

var utils = require('./../utils');
var settle = require('./../core/settle');
var buildURL = require('./../helpers/buildURL');
var buildFullPath = require('../core/buildFullPath');
var parseHeaders = require('./../helpers/parseHeaders');
var isURLSameOrigin = require('./../helpers/isURLSameOrigin');
var createError = require('../core/createError');

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    var fullPath = buildFullPath(config.baseURL, config.url);
    request.open(config.method.toUpperCase(), buildURL(fullPath, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request.onreadystatechange = function handleLoad() {
      if (!request || request.readyState !== 4) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        status: request.status,
        statusText: request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle browser request cancellation (as opposed to a manual cancellation)
    request.onabort = function handleAbort() {
      if (!request) {
        return;
      }

      reject(createError('Request aborted', config, 'ECONNABORTED', request));

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      var timeoutErrorMessage = 'timeout of ' + config.timeout + 'ms exceeded';
      if (config.timeoutErrorMessage) {
        timeoutErrorMessage = config.timeoutErrorMessage;
      }
      reject(createError(timeoutErrorMessage, config, 'ECONNABORTED',
        request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = require('./../helpers/cookies');

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(fullPath)) && config.xsrfCookieName ?
        cookies.read(config.xsrfCookieName) :
        undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (!utils.isUndefined(config.withCredentials)) {
      request.withCredentials = !!config.withCredentials;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};

},{"./../utils":"../../node_modules/axios/lib/utils.js","./../core/settle":"../../node_modules/axios/lib/core/settle.js","./../helpers/buildURL":"../../node_modules/axios/lib/helpers/buildURL.js","../core/buildFullPath":"../../node_modules/axios/lib/core/buildFullPath.js","./../helpers/parseHeaders":"../../node_modules/axios/lib/helpers/parseHeaders.js","./../helpers/isURLSameOrigin":"../../node_modules/axios/lib/helpers/isURLSameOrigin.js","../core/createError":"../../node_modules/axios/lib/core/createError.js","./../helpers/cookies":"../../node_modules/axios/lib/helpers/cookies.js"}],"../../node_modules/process/browser.js":[function(require,module,exports) {

// shim for using process in browser
var process = module.exports = {}; // cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
  throw new Error('setTimeout has not been defined');
}

function defaultClearTimeout() {
  throw new Error('clearTimeout has not been defined');
}

(function () {
  try {
    if (typeof setTimeout === 'function') {
      cachedSetTimeout = setTimeout;
    } else {
      cachedSetTimeout = defaultSetTimout;
    }
  } catch (e) {
    cachedSetTimeout = defaultSetTimout;
  }

  try {
    if (typeof clearTimeout === 'function') {
      cachedClearTimeout = clearTimeout;
    } else {
      cachedClearTimeout = defaultClearTimeout;
    }
  } catch (e) {
    cachedClearTimeout = defaultClearTimeout;
  }
})();

function runTimeout(fun) {
  if (cachedSetTimeout === setTimeout) {
    //normal enviroments in sane situations
    return setTimeout(fun, 0);
  } // if setTimeout wasn't available but was latter defined


  if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
    cachedSetTimeout = setTimeout;
    return setTimeout(fun, 0);
  }

  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedSetTimeout(fun, 0);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
      return cachedSetTimeout.call(null, fun, 0);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
      return cachedSetTimeout.call(this, fun, 0);
    }
  }
}

function runClearTimeout(marker) {
  if (cachedClearTimeout === clearTimeout) {
    //normal enviroments in sane situations
    return clearTimeout(marker);
  } // if clearTimeout wasn't available but was latter defined


  if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
    cachedClearTimeout = clearTimeout;
    return clearTimeout(marker);
  }

  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedClearTimeout(marker);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
      return cachedClearTimeout.call(null, marker);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
      // Some versions of I.E. have different rules for clearTimeout vs setTimeout
      return cachedClearTimeout.call(this, marker);
    }
  }
}

var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
  if (!draining || !currentQueue) {
    return;
  }

  draining = false;

  if (currentQueue.length) {
    queue = currentQueue.concat(queue);
  } else {
    queueIndex = -1;
  }

  if (queue.length) {
    drainQueue();
  }
}

function drainQueue() {
  if (draining) {
    return;
  }

  var timeout = runTimeout(cleanUpNextTick);
  draining = true;
  var len = queue.length;

  while (len) {
    currentQueue = queue;
    queue = [];

    while (++queueIndex < len) {
      if (currentQueue) {
        currentQueue[queueIndex].run();
      }
    }

    queueIndex = -1;
    len = queue.length;
  }

  currentQueue = null;
  draining = false;
  runClearTimeout(timeout);
}

process.nextTick = function (fun) {
  var args = new Array(arguments.length - 1);

  if (arguments.length > 1) {
    for (var i = 1; i < arguments.length; i++) {
      args[i - 1] = arguments[i];
    }
  }

  queue.push(new Item(fun, args));

  if (queue.length === 1 && !draining) {
    runTimeout(drainQueue);
  }
}; // v8 likes predictible objects


function Item(fun, array) {
  this.fun = fun;
  this.array = array;
}

Item.prototype.run = function () {
  this.fun.apply(null, this.array);
};

process.title = 'browser';
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues

process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) {
  return [];
};

process.binding = function (name) {
  throw new Error('process.binding is not supported');
};

process.cwd = function () {
  return '/';
};

process.chdir = function (dir) {
  throw new Error('process.chdir is not supported');
};

process.umask = function () {
  return 0;
};
},{}],"../../node_modules/axios/lib/defaults.js":[function(require,module,exports) {
var process = require("process");
'use strict';

var utils = require('./utils');
var normalizeHeaderName = require('./helpers/normalizeHeaderName');

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = require('./adapters/xhr');
  } else if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
    // For node use HTTP adapter
    adapter = require('./adapters/http');
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Accept');
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;

},{"./utils":"../../node_modules/axios/lib/utils.js","./helpers/normalizeHeaderName":"../../node_modules/axios/lib/helpers/normalizeHeaderName.js","./adapters/xhr":"../../node_modules/axios/lib/adapters/xhr.js","./adapters/http":"../../node_modules/axios/lib/adapters/xhr.js","process":"../../node_modules/process/browser.js"}],"../../node_modules/axios/lib/core/dispatchRequest.js":[function(require,module,exports) {
'use strict';

var utils = require('./../utils');
var transformData = require('./transformData');
var isCancel = require('../cancel/isCancel');
var defaults = require('../defaults');

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};

},{"./../utils":"../../node_modules/axios/lib/utils.js","./transformData":"../../node_modules/axios/lib/core/transformData.js","../cancel/isCancel":"../../node_modules/axios/lib/cancel/isCancel.js","../defaults":"../../node_modules/axios/lib/defaults.js"}],"../../node_modules/axios/lib/core/mergeConfig.js":[function(require,module,exports) {
'use strict';

var utils = require('../utils');

/**
 * Config-specific merge-function which creates a new config-object
 * by merging two configuration objects together.
 *
 * @param {Object} config1
 * @param {Object} config2
 * @returns {Object} New object resulting from merging config2 to config1
 */
module.exports = function mergeConfig(config1, config2) {
  // eslint-disable-next-line no-param-reassign
  config2 = config2 || {};
  var config = {};

  var valueFromConfig2Keys = ['url', 'method', 'params', 'data'];
  var mergeDeepPropertiesKeys = ['headers', 'auth', 'proxy'];
  var defaultToConfig2Keys = [
    'baseURL', 'url', 'transformRequest', 'transformResponse', 'paramsSerializer',
    'timeout', 'withCredentials', 'adapter', 'responseType', 'xsrfCookieName',
    'xsrfHeaderName', 'onUploadProgress', 'onDownloadProgress',
    'maxContentLength', 'validateStatus', 'maxRedirects', 'httpAgent',
    'httpsAgent', 'cancelToken', 'socketPath'
  ];

  utils.forEach(valueFromConfig2Keys, function valueFromConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    }
  });

  utils.forEach(mergeDeepPropertiesKeys, function mergeDeepProperties(prop) {
    if (utils.isObject(config2[prop])) {
      config[prop] = utils.deepMerge(config1[prop], config2[prop]);
    } else if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (utils.isObject(config1[prop])) {
      config[prop] = utils.deepMerge(config1[prop]);
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });

  utils.forEach(defaultToConfig2Keys, function defaultToConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });

  var axiosKeys = valueFromConfig2Keys
    .concat(mergeDeepPropertiesKeys)
    .concat(defaultToConfig2Keys);

  var otherKeys = Object
    .keys(config2)
    .filter(function filterAxiosKeys(key) {
      return axiosKeys.indexOf(key) === -1;
    });

  utils.forEach(otherKeys, function otherKeysDefaultToConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });

  return config;
};

},{"../utils":"../../node_modules/axios/lib/utils.js"}],"../../node_modules/axios/lib/core/Axios.js":[function(require,module,exports) {
'use strict';

var utils = require('./../utils');
var buildURL = require('../helpers/buildURL');
var InterceptorManager = require('./InterceptorManager');
var dispatchRequest = require('./dispatchRequest');
var mergeConfig = require('./mergeConfig');

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = arguments[1] || {};
    config.url = arguments[0];
  } else {
    config = config || {};
  }

  config = mergeConfig(this.defaults, config);

  // Set config.method
  if (config.method) {
    config.method = config.method.toLowerCase();
  } else if (this.defaults.method) {
    config.method = this.defaults.method.toLowerCase();
  } else {
    config.method = 'get';
  }

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

Axios.prototype.getUri = function getUri(config) {
  config = mergeConfig(this.defaults, config);
  return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;

},{"./../utils":"../../node_modules/axios/lib/utils.js","../helpers/buildURL":"../../node_modules/axios/lib/helpers/buildURL.js","./InterceptorManager":"../../node_modules/axios/lib/core/InterceptorManager.js","./dispatchRequest":"../../node_modules/axios/lib/core/dispatchRequest.js","./mergeConfig":"../../node_modules/axios/lib/core/mergeConfig.js"}],"../../node_modules/axios/lib/cancel/Cancel.js":[function(require,module,exports) {
'use strict';

/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;

},{}],"../../node_modules/axios/lib/cancel/CancelToken.js":[function(require,module,exports) {
'use strict';

var Cancel = require('./Cancel');

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;

},{"./Cancel":"../../node_modules/axios/lib/cancel/Cancel.js"}],"../../node_modules/axios/lib/helpers/spread.js":[function(require,module,exports) {
'use strict';

/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};

},{}],"../../node_modules/axios/lib/axios.js":[function(require,module,exports) {
'use strict';

var utils = require('./utils');
var bind = require('./helpers/bind');
var Axios = require('./core/Axios');
var mergeConfig = require('./core/mergeConfig');
var defaults = require('./defaults');

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(mergeConfig(axios.defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = require('./cancel/Cancel');
axios.CancelToken = require('./cancel/CancelToken');
axios.isCancel = require('./cancel/isCancel');

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = require('./helpers/spread');

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;

},{"./utils":"../../node_modules/axios/lib/utils.js","./helpers/bind":"../../node_modules/axios/lib/helpers/bind.js","./core/Axios":"../../node_modules/axios/lib/core/Axios.js","./core/mergeConfig":"../../node_modules/axios/lib/core/mergeConfig.js","./defaults":"../../node_modules/axios/lib/defaults.js","./cancel/Cancel":"../../node_modules/axios/lib/cancel/Cancel.js","./cancel/CancelToken":"../../node_modules/axios/lib/cancel/CancelToken.js","./cancel/isCancel":"../../node_modules/axios/lib/cancel/isCancel.js","./helpers/spread":"../../node_modules/axios/lib/helpers/spread.js"}],"../../node_modules/axios/index.js":[function(require,module,exports) {
module.exports = require('./lib/axios');
},{"./lib/axios":"../../node_modules/axios/lib/axios.js"}],"application.js":[function(require,module,exports) {
"use strict";

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TWITCH_PLAYER = 1;
var YOUTUBE_PLAYER = 2;
var playerCount = 1;

function makeid(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;

  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}

function getOffset(el) {
  var box = el.getBoundingClientRect();
  return {
    top: box.top + window.pageYOffset - document.documentElement.clientTop,
    left: box.left + window.pageXOffset - document.documentElement.clientLeft
  };
}

function shortTitle(width) {
  var string = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

  if (!string.length) {
    return string;
  } // default - width (400) divide symbols (55)


  var defaultLengthCoeff = 7.7; // = ~400/55 = 7.3

  var strLength = +(width / string.length).toFixed(2);

  if (strLength >= defaultLengthCoeff) {
    return string;
  }

  var words = string.split(' ');
  var currLenght = 0,
      currStrLengthCoeff = 0,
      currStr = '';

  for (var i = 0; i < words.length; i++) {
    currLenght = words[i].length + currLenght;
    currLenght++; // add space

    currStrLengthCoeff = +(width / currLenght).toFixed(2);
    currStr = currStr + ' ' + words[i];

    if (currStrLengthCoeff <= defaultLengthCoeff) {
      return currStr + '...';
    }
  }

  return currStr + '...';
}

function getHeight(el) {
  var styles = window.getComputedStyle(el);
  var height = el.offsetHeight;
  var borderTopWidth = parseFloat(styles.borderTopWidth);
  var borderBottomWidth = parseFloat(styles.borderBottomWidth);
  var paddingTop = parseFloat(styles.paddingTop);
  var paddingBottom = parseFloat(styles.paddingBottom);
  return height - borderBottomWidth - borderTopWidth - paddingTop - paddingBottom;
}

function playerControlsStyles(contentId, height) {
  return "\n    #container .player-controls {\n      position: absolute;\n      top: 0;\n      left: 0;\n      z-index: 3;\n      width: 100%;\n      height: ".concat(height, "px;\n    }\n    #container .player-controls__buttons {\n        width: 100%;\n        height: 100%;\n        background-image: linear-gradient(\n            to top,\n            rgba(0,0,0,0.4) 0px,\n            rgba(0,0,0,0.4) 30px,\n            rgba(0,0,0,0) 70px\n        );\n        transition: opacity 0.2s ease;\n    }\n    #container .player-controls__button {\n        position: absolute;\n        bottom: 5px;\n        width: 40px;\n        height: 40px;\n        background-size: contain;\n        background-position: center;\n        background-repeat: no-repeat;\n        cursor: pointer;\n    }\n    #container .player-controls__button::after {\n      opacity: 0.01;\n      display: block;\n      content: \"\";\n      position: absolute;\n      top: 0;\n      left: 0;\n      width: 100%;\n      height: 100%;\n      background-size: contain;\n      background-position: center;\n      background-repeat: no-repeat;\n    }\n\n    #container .player-controls__button._play {\n        left: 5px;\n        background-image: url('../images/ui/play.svg');\n        background-size: 25px auto;\n    }\n\n    #container .player-controls__button._play::after {\n        background-image: url('../images/ui/pause.svg');\n        background-size: 25px auto;\n    }\n\n    #container .player-controls__button._fullscreen {\n        right: 60px;\n        background-image: url('../images/ui/enter-fullscreen.svg');\n        background-size: 25px auto;\n    }\n\n    #container .player-controls__button._fullscreen::after {\n      background-image: url('../images/ui/exit-fullscreen.svg');\n      background-size: 25px auto;\n    }\n\n    #container .player-controls__button._mute {\n        right: 10px;\n        background-image: url('../images/ui/sound.svg');\n        background-size: 28px auto;\n    }\n\n    #container .player-controls__button._mute::after {\n        background-image: url('../images/ui/sound-disabled.svg');\n        background-size: 38px auto;\n        background-position: center left 6px;\n    }\n\n    #").concat(contentId, ".vjs-playing .player-controls__button._play {\n        background-image: none;\n    }\n\n    #").concat(contentId, ".vjs-playing .player-controls__button._play::after {\n      opacity: 1;\n    }\n\n    #").concat(contentId, ".vjs-fullscreen .player-controls__button._fullscreen {\n            background-image: none;\n    }\n\n    #").concat(contentId, ".vjs-fullscreen .player-controls__button._fullscreen::after {\n      opacity: 1;\n    }\n\n    #").concat(contentId, ".vjs-muted .player-controls__button._mute {\n            background-image: none;\n    }\n    #").concat(contentId, ".vjs-muted .player-controls__button._mute::after {\n        opacity: 1;\n    } \n    #").concat(contentId, ".vjs-has-started.vjs-user-inactive .player-controls__buttons {\n      opacity: 0;\n    }\n  ");
}
/**
 * Handles video player functionality.
 */


var VideoPlayer = function VideoPlayer(_ref, playCb) {
  var autoplay = _ref.autoplay,
      mode = _ref.mode,
      title = _ref.title,
      twitchType = _ref.twitchType,
      width = _ref.width,
      height = _ref.height,
      widthFloated = _ref.widthFloated,
      heightFloated = _ref.heightFloated;
  var videoPlayerId = "videoplayer_".concat(makeid(10), "_").concat(playerCount);
  var acContainerId = "adcontainer_".concat(makeid(10), "_").concat(playerCount);
  var contentId = "content_".concat(makeid(10), "_").concat(playerCount);
  var closeButtonId = "closeButton_".concat(makeid(10), "_").concat(playerCount);
  var container = document.getElementById('container');
  container.setAttribute('data-mode', mode);
  container.innerHTML = "\n    <div id=\"".concat(videoPlayerId, "\">\n      <div id=\"").concat(closeButtonId, "\"></div>\n      <div id=\"").concat(contentId, "\">\n      </div>\n      <div id=\"").concat(acContainerId, "\" style=\"position: absolute; top: 0px; left: 0px;\">\n      </div>\n    </div>");
  this.contentPlayer = document.getElementById(contentId);
  this.adContainer = document.getElementById(acContainerId);
  this.videoPlayerContainer_ = document.getElementById(videoPlayerId);
  this.panelTitle = title;
  this.width = width;
  this.height = height;
  this.widthFloated = widthFloated;
  this.heightFloated = heightFloated;
  this.playerType = null; // 1 - twitch, 2 - youtube

  this.firstInit = true;
  this.firstPress = false;
  this.mixedFloated = false;
  this.isOnlineStream = true;
  this.playerStatusPanelClass = '';
  var that = this;

  if (document.body.clientWidth < 700) {
    this.widthFloated = 280;
    this.heightFloated = 160;
  }

  if (mode === 'fixed' || mode === 'mixed') {
    var additionalStyles = document.createElement('style');
    var hiddenClass = "hidden_".concat(makeid(10), "_").concat(playerCount);
    var fixedClass = "fixed_".concat(makeid(10), "_").concat(playerCount);
    var playerStatusPanelClass = "player-status-panel_".concat(makeid(10), "_").concat(playerCount);
    this.playerStatusPanelClass = playerStatusPanelClass;
    var statusPanel = document.createElement('div');

    if (twitchType) {
      statusPanel.setAttribute('style', 'background-image: none; padding: 9px 15px 9px 9px;');
    }

    statusPanel.className = playerStatusPanelClass;
    statusPanel.innerHTML = "<span class=\"js-video-title\">".concat(this.panelTitle, "</span>");
    this.videoPlayerContainer_.prepend(statusPanel);
    additionalStyles.className = 'xd-s';
    additionalStyles.innerHTML = "\n    #".concat(videoPlayerId, ".").concat(fixedClass, " .").concat(playerStatusPanelClass, " {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      \n    }\n\n    .").concat(playerStatusPanelClass, " {\n      display: none;\n      box-sizing: border-box;\n      justify-content: flex-start;\n      align-items: center;\n      position: relative;\n      min-height: 27px;\n      width: 100%;\n      overflow: hidden;\n      padding: 9px 15px 9px 47px;\n      text-align: left;\n      font-family: Roboto, Helvetica, Arial, sans-serif;\n      font-size: 11px;\n      line-height: 1;\n      font-weight: 500;\n      color: #333333;\n      background-color: #fff;\n      background-image: url('https://player.uplify.app/images/icons/live.svg');\n      background-position: left 7px center;\n      background-size: auto 16px;\n      background-repeat: no-repeat;\n  \n    }\n    .").concat(playerStatusPanelClass, " span {\n        white-space: nowrap;\n    }\n    .").concat(playerStatusPanelClass, "::after {\n      display: block;\n      content: \"\";\n      position: absolute;\n      z-index: 2;\n      top: 0;\n      right: 0;\n      width: 40px;\n      height: 100%;\n      background-image: linear-gradient(\n          to left,\n          rgba(255,255,255,1) 0px,\n          rgba(255,255,255,1) 18px,\n          rgba(255,255,255,0) 25px,\n      );\n    }\n\n    #container {\n        display: none;\n        margin: 40px 0;\n        background-color: #ccc;\n    }\n   \n    #container.loaded {\n        display: block;\n    }\n    \n    #").concat(videoPlayerId, " {\n        position: relative;\n    }\n    \n    #container,\n    #").concat(videoPlayerId, ",\n    #").concat(contentId, " {\n        width: ").concat(this.widthFloated, "px;\n        height: ").concat(+this.heightFloated + 29, "px;\n    }\n\n    @media (max-width: 700px) {\n      #container,\n      #").concat(videoPlayerId, ",\n      #").concat(contentId, " {\n\n      }\n    }\n    \n    #").concat(videoPlayerId, ".").concat(fixedClass, " {\n      position: fixed;\n      z-index: 40;\n      right: 40px;\n      bottom: 135px;\n      border: 1px solid #999999;\n      border-radius: 2px 2px 0 0;\n      -webkit-box-shadow: 0px 4px 4px rgba(0,0,0,0.25);\n      box-shadow: 0px 4px 4px rgba(0,0,0,0.25);\n    }\n\n    @media (max-width: 700px) {\n      #").concat(videoPlayerId, ".").concat(fixedClass, " {\n        right: 20px;\n        bottom: 45px;\n      }\n    }\n      \n    .").concat(hiddenClass, " {\n        width: 0!important;\n        height: 0!important;\n        margin: 0!important;\n    }\n    .").concat(fixedClass, " #").concat(closeButtonId, " {\n        display: block;\n    }\n    \n    @media (max-width: 700px) {\n      #").concat(closeButtonId, " {\n        top: auto!important;\n        bottom: calc(100% + 3px);\n        left: auto!important;\n        right: -1px;\n        background-color: #fff;\n        border: 1px solid #999999;\n      }\n\n      #").concat(closeButtonId, ":hover {\n        transform: none;\n      }\n    }\n\n    #").concat(closeButtonId, " {\n        display: none;\n        position: absolute;\n        width: 30px;\n        height: 30px;\n        top: 0px;\n        left: 100%;\n        cursor: pointer;\n    }\n\n    #").concat(closeButtonId, ":hover {\n      transform: rotate(90deg);\n    }\n      \n    #").concat(closeButtonId, "::before,\n    #").concat(closeButtonId, "::after {\n        display: block;\n        content: \"\";\n        position: absolute;\n        top: calc(50% - 1px);\n        left: 5px;\n        width: 20px;\n        height: 2px;\n        background-color: #000;\n    }\n    \n    #").concat(closeButtonId, "::before {\n        transform: rotate(-45deg);\n    }\n    \n    #").concat(closeButtonId, "::after {\n        transform: rotate(45deg);\n    }}"); // ${playerControlsStyles(contentId, this.height + 30)}`;

    document.head.appendChild(additionalStyles);

    if (mode === 'fixed') {
      container.className = "".concat(hiddenClass, " loaded");
      this.videoPlayerContainer_.className = fixedClass;
      document.getElementById(acContainerId).style.top = '29px';
    } else if (mode === 'mixed') {
      container.className = "loaded";
      initScrollTrack();
    }

    function setNormalPlayer() {
      that.mixedFloated = false;
      that.videoPlayerContainer_.className = '';
      document.getElementById(contentId).getElementsByTagName('iframe')[0].width = that.width;
      document.getElementById(contentId).getElementsByTagName('iframe')[0].height = that.height;
      document.getElementById(acContainerId).style.top = '0px';
      document.getElementById(videoPlayerId).style.height = "".concat(that.height, "px");
      document.getElementById('container').style.height = "".concat(that.height, "px");
    }

    function setFixedPlayer() {
      that.mixedFloated = true;
      that.videoPlayerContainer_.className = fixedClass;
      document.getElementById(contentId).getElementsByTagName('iframe')[0].width = that.widthFloated;
      document.getElementById(contentId).getElementsByTagName('iframe')[0].height = that.heightFloated;
      document.getElementById(acContainerId).style.top = '29px';
      document.getElementById(videoPlayerId).style.height = "".concat(that.heightFloated, "px");
      document.getElementById(videoPlayerId).style.width = "".concat(that.widthFloated, "px");
      document.getElementById('container').style.height = "".concat(that.heightFloated, "px");
    }

    function scrollListenner(e) {
      if (that.player && mode === 'mixed') {
        // this.contentPlayer
        try {
          var scrlollTop = document.documentElement && document.documentElement.scrollTop || document.body.scrollTop;

          if (scrlollTop > getOffset(container).top + getHeight(that.contentPlayer) / 2) {
            setFixedPlayer();
          } else {
            setNormalPlayer();
          }
        } catch (e) {
          console.warn(e);
        }
      }
    }

    function initScrollTrack() {
      window.addEventListener('scroll', scrollListenner);
    }

    function resizeListener() {
      if (document.body.clientWidth < 700) {
        that.widthFloated = 280;
        that.heightFloated = 160;
      } else {
        that.widthFloated = widthFloated;
        that.heightFloated = heightFloated;
      }

      try {
        var scrlollTop = document.documentElement && document.documentElement.scrollTop || document.body.scrollTop;

        if (scrlollTop > getOffset(container).top + getHeight(that.contentPlayer) / 2) {
          setFixedPlayer();
        }
      } catch (e) {
        console.warn(e);
      }
    }

    window.addEventListener('resize', resizeListener);
    document.getElementById(closeButtonId).addEventListener('click', function (e) {
      window.removeEventListener('scroll', scrollListenner);

      if (mode === 'fixed') {
        container.innerHTML = '';
      } else {
        setNormalPlayer();
      }
    });
  }

  if (window.PLATFORM == 'twitch') {
    var twitchOpts = {
      channel: window.PLATFORM_ID
    };

    if (this.width && this.height && mode === 'fixed') {
      twitchOpts.width = this.widthFloated;
      twitchOpts.height = this.heightFloated;
    }

    if (this.width && this.height && mode === 'normal') {
      twitchOpts.width = this.width;
      twitchOpts.height = this.heigth;
    }

    if (this.width && this.height && mode === 'mixed') {
      twitchOpts.width = this.widthFloated;
      twitchOpts.height = this.heightFloated;
    } // var playerEl = document.getElementsByTagName('iframe').length ? document.getElementsByTagName('iframe')[0] : null;


    if (!twitchOpts.width && !twitchOpts.height) {
      // playerEl.width = '100%';
      // playerEl.height = '100%';
      if (document.getElementById('container').clientWidth) {
        this.width = document.getElementById('container').clientWidth;
        this.height = Math.round(this.width / 1.77); // 16:9
      } else {
        this.width = document.getElementById('container').parentNode.clientWidth;
        this.height = Math.round(this.width / 1.77); // 16:9
      }
    } // init player before mixed


    this.player = new Twitch.Player(contentId, twitchOpts);

    if (mode === 'mixed') {
      if (!document.getElementById(videoPlayerId).className) {
        debugger;
        document.getElementById(contentId).getElementsByTagName('iframe')[0].width = this.width;
        document.getElementById(contentId).getElementsByTagName('iframe')[0].height = this.height;
        document.getElementById(videoPlayerId).style.height = "".concat(this.height, "px");
        document.getElementById(videoPlayerId).style.width = "".concat(this.width, "px");
        document.getElementById('container').style.height = "".concat(this.height, "px"); // document.getElementById('container').style.width = `${this.width}px`;
      }
    }

    if (window.TWITCH_TYPE == 'video') {
      twitchOpts.video = window.PLATFORM_ID;
      delete twitchOpts.channel;
    }

    this.playerType = TWITCH_PLAYER;
    this.player.setVolume(0.5);

    if (mode !== 'normal') {
      document.querySelector('.js-video-title').innerText = this.panelTitle;
    }

    this.player.addEventListener(Twitch.Player.READY, function () {
      setTimeout(function () {
        if (that.firstInit) {
          if (mode === 'mixed') {
            that.adContainer.style.display = 'none';
            that.player.pause();
          } else if (mode === 'fixed') {
            that.player.setMuted(true);
            that.player.play();
          }

          that.firstInit = false;
        }

        that.player.addEventListener(Twitch.Player.PLAY, function () {
          if (!that.firstInit && !that.firstPress) {
            // that.player.pause();
            playCb();
            that.adContainer.style.display = 'block';
            that.firstPress = true;
          } // console.log('PLAY');

        });
      }, 3000);
    });
    this.player.addEventListener('online', function () {// console.log('ONLINE');
    });
  } else if (window.PLATFORM == 'youtube') {
    // window.removeEventListener('scroll', scrollListenner);
    if (document.querySelectorAll('.xd-s').length) {
      for (var i = 0; i < document.querySelectorAll('.xd-s').length; i++) {
        document.querySelectorAll('.xd-s')[i].remove();
      }
    }

    for (var _i = 0; _i < document.querySelectorAll('script').length; _i++) {
      if (document.querySelectorAll('script')[_i].src === 'https://www.youtube.com/iframe_api') {
        document.querySelectorAll('script')[_i].remove();
      }
    } // 2. This code loads the IFrame Player API code asynchronously.


    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.

    window.onYouTubeIframeAPIReady = function () {
      console.log("YouTube API Ready");
      var youtubeBlock = document.createElement('div');
      youtubeBlock.setAttribute('id', 'youtubeContainer');
      document.getElementById(contentId).append(youtubeBlock);
      that.player = new YT.Player('youtubeContainer', {
        videoId: window.PLATFORM_ID,
        //'9gb8z4R39LE',
        playerVars: {
          controls: 1,
          autoplay: 0,
          disablekb: 1,
          enablejsapi: 1,
          iv_load_policy: 3,
          // modestbranding: 1,
          showinfo: 1
        },
        // try disable for response video
        // height: window.PLAYER_HEIGHT,
        // width: window.PLAYER_WIDTH,
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      });
      that.playerType = YOUTUBE_PLAYER; //  ytLoaded = true;
    }; // 4. The API will call this function when the video player is ready.


    window.onPlayerReady = function (event) {// event.target.playVideo();
    }; // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.


    var done = false;

    window.onPlayerStateChange = function (event) {
      // if (event.data == YT.PlayerState.PLAYING && !done) {
      //   setTimeout(stopVideo, 6000);
      //   done = true;
      // }
      if (event.data == YT.PlayerState.PLAYING) {
        if (that.firstInit) {
          playCb();
          that.adContainer.style.display = 'block';
          that.firstPress = true;
          that.firstInit = false;
        }
      } // console.log('STATE', event.data);

    };

    window.stopVideo = function () {
      player.stopVideo();
    };
  }

  playerCount++;
};

VideoPlayer.prototype.setPanelTitle = function (title) {
  this.panelTitle = shortTitle(this.widthFloated, title);

  if (this.videoPlayerContainer_.getElementsByClassName('js-video-title').length) {
    this.videoPlayerContainer_.getElementsByClassName('js-video-title')[0].innerText = this.panelTitle;
  }

  if (!this.isOnlineStream) {
    this.videoPlayerContainer_.getElementsByClassName(this.playerStatusPanelClass)[0].style['background-image'] = 'none';
  }
};

VideoPlayer.prototype.hideAd = function () {
  this.adContainer.innerHTML = '';

  if (this.isMuted()) {
    this.setVolume(0);
  } else {
    this.setVolume(0.7);
  } // this.play();
  // this.adContainer.style.display = 'none';
  // console.log('ADS END');

};

VideoPlayer.prototype.preloadContent = function (contentLoadedAction) {
  // If this is the initial user action on iOS or Android device,
  // simulate playback to enable the video element for later program-triggered
  // playback.
  if (this.isMobilePlatform()) {
    this.contentPlayer.addEventListener('loadedmetadata', contentLoadedAction, false);
    this.contentPlayer.load();
  } else {
    contentLoadedAction();
  }
};

VideoPlayer.prototype.play = function () {
  if (this.playerType === TWITCH_PLAYER) {
    this.player.play();
  } else if (this.playerType === YOUTUBE_PLAYER) {
    this.player.playVideo();
  } else {
    throw new Error('Wrong player type');
  }
};

VideoPlayer.prototype.pause = function () {
  if (this.playerType === TWITCH_PLAYER) {
    this.player.pause();
  } else if (this.playerType === YOUTUBE_PLAYER) {
    this.player.pauseVideo();
  } else {
    throw new Error('Wrong player type');
  }
};

VideoPlayer.prototype.isPaused = function () {
  if (this.playerType === TWITCH_PLAYER) {
    return this.player.isPaused();
  } else if (this.playerType === YOUTUBE_PLAYER) {
    // -1 – unstarted
    // 0 – ended
    // 1 – playing
    // 2 – paused
    // 3 – buffering
    // 5 – video cued
    return this.player.getPlayerState() == 2;
  } else {
    throw new Error('Wrong player type');
  }
};

VideoPlayer.prototype.isMuted = function () {
  if (this.playerType === TWITCH_PLAYER) {
    return this.player.getMuted();
  } else if (this.playerType === YOUTUBE_PLAYER) {
    return this.player.isMuted();
  } else {
    throw new Error('Wrong player type');
  }
};

VideoPlayer.prototype.isMobilePlatform = function () {
  return this.contentPlayer.paused && (navigator.userAgent.match(/(iPod|iPhone|iPad)/) || navigator.userAgent.toLowerCase().indexOf('android') > -1);
};

VideoPlayer.prototype.resize = function (position, top, left, width, height) {
  this.videoPlayerContainer_.style.position = position;
  this.videoPlayerContainer_.style.top = top + 'px';
  this.videoPlayerContainer_.style.left = left + 'px';
  this.videoPlayerContainer_.style.width = width + 'px';
  this.videoPlayerContainer_.style.height = height + 'px';
  this.contentPlayer.style.width = width + 'px';
  this.contentPlayer.style.height = height + 'px';
};

VideoPlayer.prototype.registerVideoEndedCallback = function (callback) {
  this.contentPlayer.addEventListener('ended', callback, false);
};

VideoPlayer.prototype.setVolume = function (volume) {
  if (this.playerType === TWITCH_PLAYER) {
    return this.player.setVolume(volume);
  } else if (this.playerType === YOUTUBE_PLAYER) {
    return this.player.setVolume(volume * 10);
  } else {
    throw new Error('Wrong player type');
  }
};
/**
 * Shows how to use the IMA SDK to request and display ads.
 */


var Ads = function Ads(application, videoPlayer, showMode) {
  this.application_ = application;
  this.videoPlayer_ = videoPlayer;
  this.showMode = showMode;
  this.contentCompleteCalled_ = false;
  this.adDisplayContainer_ = new google.ima.AdDisplayContainer(this.videoPlayer_.adContainer, this.videoPlayer_.contentPlayer);
  this.adsLoader_ = new google.ima.AdsLoader(this.adDisplayContainer_);
  var mode = google.ima.ImaSdkSettings.VpaidMode.INSECURE;
  this.adsLoader_.getSettings().setVpaidMode(mode);
  this.adsManager_ = null;
  this.adsLoader_.addEventListener(google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, this.onAdsManagerLoaded_, false, this);
  this.adsLoader_.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, this.onAdError_, false, this);
}; // On iOS and Android devices, video playback must begin in a user action.
// AdDisplayContainer provides a initialize() API to be called at appropriate
// time.
// This should be called when the user clicks or taps.


Ads.prototype.initialUserAction = function () {
  this.adDisplayContainer_.initialize();
};

Ads.prototype.requestXml = function (adXML) {
  // Mobile calls requestXml when video source changes.
  // The VPAID ad will change the video source.
  if (this.adsManager_ != null) {
    return;
  }

  var adsRequest = new google.ima.AdsRequest();
  adsRequest.adTagUrl = ''; // No url, using xml instead

  adsRequest.adsResponse = adXML;

  if (this.showMode === 'fixed' || this.showMode === 'mixed' && this.videoPlayer_.mixedFloated === true) {
    adsRequest.linearAdSlotWidth = this.videoPlayer_.widthFloated;
    adsRequest.linearAdSlotHeight = this.videoPlayer_.heightFloated;
    adsRequest.nonLinearAdSlotWidth = this.videoPlayer_.widthFloated;
    adsRequest.nonLinearAdSlotHeight = this.videoPlayer_.heightFloated;
  } else {
    adsRequest.linearAdSlotWidth = this.videoPlayer_.width;
    adsRequest.linearAdSlotHeight = this.videoPlayer_.height;
    adsRequest.nonLinearAdSlotWidth = this.videoPlayer_.width;
    adsRequest.nonLinearAdSlotHeight = this.videoPlayer_.height;
  }

  this.adsLoader_.requestAds(adsRequest);
};

Ads.prototype.pause = function () {
  if (this.adsManager_) {
    this.adsManager_.pause();
  }
};

Ads.prototype.resume = function () {
  if (this.adsManager_) {
    this.adsManager_.resume();
  }
};

Ads.prototype.resize = function (width, height) {
  if (this.adsManager_) {
    this.adsManager_.resize(width, height, google.ima.ViewMode.FULLSCREEN);
  }
};

Ads.prototype.contentEnded = function () {
  this.contentCompleteCalled_ = true;
  this.adsLoader_.contentComplete();
  this.videoPlayer_.hideAd();
};

Ads.prototype.onAdsManagerLoaded_ = function (adsManagerLoadedEvent) {
  this.application_.log('Ads loaded.');
  this.adsManager_ = adsManagerLoadedEvent.getAdsManager(this.videoPlayer_.contentPlayer);

  if (this.videoPlayer_.isMuted()) {
    this.adsManager_.setVolume(0);
  } else {
    this.adsManager_.setVolume(0.7);
  }

  this.processAdsManager_(this.adsManager_);
};

Ads.prototype.clear = function () {
  this.adsLoader_.removeEventListener(google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, this.onAdsManagerLoaded_, false, this);
  this.adsLoader_.removeEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, this.onAdError_, false, this);

  if (this.adsManager_) {
    this.adsManager_.removeEventListener(google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED, this.onContentPauseRequested_, false, this);
    this.adsManager_.removeEventListener(google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, this.onContentResumeRequested_, false, this);
    this.adsManager_.removeEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, this.onAdError_, false, this);
    var events = [google.ima.AdEvent.Type.ALL_ADS_COMPLETED, google.ima.AdEvent.Type.CLICK, google.ima.AdEvent.Type.COMPLETE, google.ima.AdEvent.Type.FIRST_QUARTILE, google.ima.AdEvent.Type.LOADED, google.ima.AdEvent.Type.MIDPOINT, google.ima.AdEvent.Type.PAUSED, google.ima.AdEvent.Type.STARTED, google.ima.AdEvent.Type.THIRD_QUARTILE];

    for (var index in events) {
      this.adsManager_.removeEventListener(events[index], this.onAdEvent_, false, this);
    }
  }
};

Ads.prototype.processAdsManager_ = function (adsManager) {
  // Attach the pause/resume events.
  adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED, this.onContentPauseRequested_, false, this);
  adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, this.onContentResumeRequested_, false, this); // Handle errors.

  adsManager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, this.onAdError_, false, this);
  var events = [google.ima.AdEvent.Type.ALL_ADS_COMPLETED, google.ima.AdEvent.Type.CLICK, google.ima.AdEvent.Type.COMPLETE, google.ima.AdEvent.Type.FIRST_QUARTILE, google.ima.AdEvent.Type.LOADED, google.ima.AdEvent.Type.MIDPOINT, google.ima.AdEvent.Type.PAUSED, google.ima.AdEvent.Type.STARTED, google.ima.AdEvent.Type.THIRD_QUARTILE];

  for (var index in events) {
    adsManager.addEventListener(events[index], this.onAdEvent_, false, this);
  }

  var initWidth, initHeight;

  if (this.application_.fullscreen) {
    initWidth = this.application_.fullscreenWidth;
    initHeight = this.application_.fullscreenHeight;
  } else {
    if (this.showMode === 'fixed' || this.showMode === 'mixed' && this.videoPlayer_.mixedFloated === true) {
      initWidth = this.videoPlayer_.widthFloated;
      initHeight = this.videoPlayer_.heightFloated;
    } else {
      initWidth = this.videoPlayer_.width;
      initHeight = this.videoPlayer_.height;
    }
  }

  adsManager.init(initWidth, initHeight, google.ima.ViewMode.NORMAL);
  adsManager.start();
};

Ads.prototype.onContentPauseRequested_ = function (adErrorEvent) {// DISABLE PAUSE HERE - MOVE TO onAdEventStarted
  // this.application_.pauseForAd();
  // console.log('VIDEO MUST BE PAUSED')
};

Ads.prototype.onContentResumeRequested_ = function (adErrorEvent) {
  // Without this check the video starts over from the beginning on a
  // post-roll's CONTENT_RESUME_REQUESTED
  if (!this.contentCompleteCalled_) {
    this.application_.resumeAfterAd();
  }
};

Ads.prototype.onAdEvent_ = function (adEvent) {
  this.application_.log('Ad event: ' + adEvent.type);

  if (adEvent.type == google.ima.AdEvent.Type.STARTED) {
    this.application_.pauseForAd();
  }

  if (adEvent.type == google.ima.AdEvent.Type.CLICK) {
    this.application_.adClicked();
  }

  if (adEvent.type == google.ima.AdEvent.Type.COMPLETE || adEvent.type == google.ima.AdEvent.Type.ALL_ADS_COMPLETED) {
    this.videoPlayer_.hideAd();
  }
};

Ads.prototype.onAdError_ = function (adErrorEvent) {
  this.application_.log('Ad error: ' + adErrorEvent.getError().toString());

  if (this.adsManager_) {
    this.adsManager_.destroy();
  }

  this.application_.resumeAfterAd();
  this.videoPlayer_.hideAd();
};
/**
 * Handles user interaction and creates the player and ads controllers.
 */


var Application = function Application(_ref2) {
  var platform = _ref2.platform,
      twitchType = _ref2.twitchType,
      platformId = _ref2.platformId,
      sectionId = _ref2.sectionId,
      width = _ref2.width,
      height = _ref2.height,
      _ref2$widthFloated = _ref2.widthFloated,
      widthFloated = _ref2$widthFloated === void 0 ? 400 : _ref2$widthFloated,
      _ref2$heightFloated = _ref2.heightFloated,
      heightFloated = _ref2$heightFloated === void 0 ? 225 : _ref2$heightFloated,
      interval = _ref2.interval,
      _ref2$autoplay = _ref2.autoplay,
      autoplay = _ref2$autoplay === void 0 ? false : _ref2$autoplay,
      _ref2$mode = _ref2.mode,
      mode = _ref2$mode === void 0 ? 'normal' : _ref2$mode,
      _ref2$title = _ref2.title,
      title = _ref2$title === void 0 ? document.title : _ref2$title;

  if (!platform || ['twitch', 'youtube'].indexOf(platform) === -1) {
    throw new Error('Unknown platform');
  }

  if (platform === 'twtich' && !twtichType) {
    throw new Error('Wrong twitch type');
  }

  if (!platformId) {
    throw new Error('Wrong video indentificator');
  }

  if (!sectionId) {
    throw new Error('Wrong section id from between');
  } // if (!w || !h) {
  //   throw new Error('Width or height of a player is empty');
  // }


  window.PLATFORM = platform;
  window.TWITCH_TYPE = twitchType;
  window.PLATFORM_ID = platformId;
  window.SECTION_ID = sectionId;
  window.PLAYER_WIDTH = width;
  window.PLAYER_HEIGHT = height;
  window.INTERVAL = +interval > 60 ? interval : 60; // this.playButton_ = document.getElementById('playpause');
  // this.loadAdButton = document.getElementById('loadad');
  // this.loadAdButton.addEventListener('click', () => {
  //   this.makeRequest_('https://match.ads.betweendigital.com/vpaid_prod?s='+
  //   window.SECTION_ID + '&maxd=300&mind=1&w=' + window.PLAYER_WIDTH + '&h=' + window.PLAYER_HEIGHT)
  //   .then(res => {
  //     this.setXml_(res.data);
  //   })
  //   .catch(err => {
  //     this.log(err);
  //   });
  // }, false);
  // this.playButton_.addEventListener(
  //     'click',
  //     this.bind_(this, this.onClick_),
  //     false);
  // this.fullscreenButton_ = document.getElementById('fullscreen');
  // this.fullscreenButton_.addEventListener(
  //     'click',
  //     this.bind_(this, this.onFullscreenClick_),
  //     false);

  this.fullscreenWidth = null;
  this.fullscreenHeight = null; // var fullScreenEvents = [
  //     'fullscreenchange',
  //     'mozfullscreenchange',
  //     'webkitfullscreenchange'];
  // for (key in fullScreenEvents) {
  //   document.addEventListener(
  //       fullScreenEvents[key],
  //       this.bind_(this, this.onFullscreenChange_),
  //       false);
  // }

  this.playing_ = false;
  this.adsActive_ = false;
  this.adsDone_ = false;
  this.fullscreen = false;
  this.lastAdShow = null;
  var that = this;
  this.videoPlayer_ = new VideoPlayer({
    autoplay: autoplay,
    mode: mode,
    title: title,
    twitchType: twitchType,
    width: width,
    height: height,
    widthFloated: widthFloated,
    heightFloated: heightFloated
  }, function () {
    that.onClick_();
    that.lastAdShow = Math.round(new Date().getTime() / 1000);
  });
  (0, _axios.default)("https://api.uplify.app/v1/twitch/checkOnline?username=".concat(platformId)).then(function (r) {
    if (r.data && r.data.stream && r.data.stream.title) {
      that.videoPlayer_.setPanelTitle(r.data.stream.title);
    } else if (r.data && r.data.video && r.data.video.title) {
      that.videoPlayer_.isOnlineStream = false;
      that.videoPlayer_.setPanelTitle(r.data.video.title);
    } else {
      that.videoPlayer_.isOnlineStream = false;
      that.videoPlayer_.setPanelTitle(document.title);
    }
  }).catch(function (e) {
    that.videoPlayer_.isOnlineStream = false;
    that.videoPlayer_.setPanelTitle(document.title);
    console.warn(e, 'Cannot get stream data');
  });
  this.ads_ = new Ads(this, this.videoPlayer_, mode);
  this.adXml_ = '';
  this.videoPlayer_.registerVideoEndedCallback(this.bind_(this, this.onContentEnded_));
  this.httpRequest_ = null;
  var ref = window.location.host;

  if (window.location.host.indexOf('esforce') !== -1) {
    ref = 'cybersport.ru';
  }

  var url = 'https://match.ads.betweendigital.com/vpaid_prod?s=' + window.SECTION_ID + '&ref=' + ref + '&client_auction=1'; // only section
  // !window.PLAYER_WIDTH && !window.PLAYER_HEIGHT ? 
  //   'https://match.ads.betweendigital.com/vpaid_prod?s=' + window.SECTION_ID + '&maxd=300&mind=1&ref=' + window.location.host :
  //   'https://match.ads.betweendigital.com/vpaid_prod?s=' + window.SECTION_ID + '&maxd=300&mind=1&w=' + window.PLAYER_WIDTH + '&h=' + window.PLAYER_HEIGHT + '&ref=' + window.location.host;

  this.makeRequest_(url).then(function (res) {
    that.setXml_(res.data);
  }).catch(function (err) {
    that.log(err);
  });
  this.isFirstAdShow = false;
  setInterval(function () {
    var now = Math.round(new Date().getTime() / 1000);
    var interval = window.INTERVAL * 1;

    if (that.lastAdShow && now - interval > that.lastAdShow && !that.videoPlayer_.isPaused()) {
      try {
        that.ads_.clear();
        that.ads_ = new Ads(that, that.videoPlayer_, mode);
        that.adsActive_ = false;
        that.adsDone_ = false;
        that.playing_ = false;
        that.onClick_();
      } catch (e) {
        console.warn('Cannot run Ad', e);
      }

      that.lastAdShow = Math.round(new Date().getTime() / 1000);
    }
  }, 10 * 1000);
};

Application.prototype.log = function (message) {
  console.log(message); // this.console_.innerHTML = this.console_.innerHTML + '<br/>' + message;
};

Application.prototype.resumeAfterAd = function () {
  this.videoPlayer_.play();
  this.adsActive_ = false;
  this.updateChrome_();
};
/**
 * Begin show ad
 */


Application.prototype.pauseForAd = function () {
  this.videoPlayer_.adContainer.style.display = 'block';
  this.adsActive_ = true;
  this.playing_ = true;
  this.videoPlayer_.setVolume(0); // DISABLINIG PAUSE STREAM
  // this.videoPlayer_.pause();
  // console.log('VIDOEPLAYER PAUSED');

  this.updateChrome_();
};

Application.prototype.adClicked = function () {
  this.updateChrome_();
};

Application.prototype.bind_ = function (thisObj, fn) {
  return function () {
    fn.apply(thisObj, arguments);
  };
};

Application.prototype.makeRequest_ = function (url) {
  return _axios.default.get(url);

  if (window.XMLHttpRequest) {
    this.httpRequest_ = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    try {
      this.httpRequest_ = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      this.httpRequest_ = new ActiveXObject("Microsoft.XMLHTTP");
    }
  }

  this.httpRequest_.onreadystatechange = this.bind_(this, this.setXml_);
  this.httpRequest_.open('GET', url);
  this.httpRequest_.send();
};

Application.prototype.setXml_ = function (data) {
  this.xmlBox_ = data; // this.xmlBox_ = this.httpRequest_.responseText;
};

Application.prototype.onClick_ = function () {
  if (!this.adsDone_) {
    this.log('Begin ad show...');

    if (this.xmlBox_ == '') {
      this.log("Error: please fill in xml");
      return;
    } else {
      this.adXml_ = this.xmlBox_;
    } // The user clicked/tapped - inform the ads controller that this code
    // is being run in a user action thread.


    this.ads_.initialUserAction(); // At the same time, initialize the content player as well.
    // When content is loaded, we'll issue the ad request to prevent it
    // from interfering with the initialization. See
    // https://developers.google.com/interactive-media-ads/docs/sdks/html5/v3/ads#iosvideo
    // for more information.

    this.videoPlayer_.preloadContent(this.bind_(this, this.loadAds_));
    this.adsDone_ = true;
    return;
  }

  if (this.adsActive_) {
    if (this.playing_) {
      this.ads_.pause();
    } else {
      this.ads_.resume();
    }
  } else {
    if (this.playing_) {
      this.videoPlayer_.pause();
    } else {
      this.videoPlayer_.play();
    }
  }

  this.playing_ = !this.playing_;
  this.updateChrome_();
};

Application.prototype.onFullscreenClick_ = function () {
  if (this.fullscreen) {
    // The video is currently in fullscreen mode
    var cancelFullscreen = document.exitFullScreen || document.webkitCancelFullScreen || document.mozCancelFullScreen;

    if (cancelFullscreen) {
      cancelFullscreen.call(document);
    } else {
      this.onFullscreenChange_();
    }
  } else {
    // Try to enter fullscreen mode in the browser
    var requestFullscreen = document.documentElement.requestFullScreen || document.documentElement.webkitRequestFullScreen || document.documentElement.mozRequestFullScreen;

    if (requestFullscreen) {
      this.fullscreenWidth = window.screen.width;
      this.fullscreenHeight = window.screen.height;
      requestFullscreen.call(document.documentElement);
    } else {
      this.fullscreenWidth = window.innerWidth;
      this.fullscreenHeight = window.innerHeight;
      this.onFullscreenChange_();
    }
  }

  requestFullscreen.call(document.documentElement);
};

Application.prototype.updateChrome_ = function () {
  if (this.playing_) {// this.playButton_.textContent = 'II';
  } else {// Unicode play symbol.
      // this.playButton_.textContent = String.fromCharCode(9654);
    }
};

Application.prototype.loadAds_ = function () {
  this.ads_.requestXml(this.adXml_);
};

Application.prototype.onFullscreenChange_ = function () {
  if (this.fullscreen) {
    // The user just exited fullscreen
    // Resize the ad container
    this.ads_.resize(this.videoPlayer_.width, this.videoPlayer_.height); // Return the video to its original size and position

    this.videoPlayer_.resize('relative', '', '', this.videoPlayer_.width, this.videoPlayer_.height);
    this.fullscreen = false;
  } else {
    // The fullscreen button was just clicked
    // Resize the ad container
    var width = this.fullscreenWidth;
    var height = this.fullscreenHeight;
    this.makeAdsFullscreen_(); // Make the video take up the entire screen

    this.videoPlayer_.resize('absolute', 0, 0, width, height);
    this.fullscreen = true;
  }
};

Application.prototype.makeAdsFullscreen_ = function () {
  this.ads_.resize(this.fullscreenWidth, this.fullscreenHeight);
};

Application.prototype.onContentEnded_ = function () {
  this.ads_.contentEnded();
};

window.UPApp = Application;
},{"axios":"../../node_modules/axios/index.js"}],"../../node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "39193" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../node_modules/parcel-bundler/src/builtins/hmr-runtime.js","application.js"], null)
//# sourceMappingURL=/application.js.map