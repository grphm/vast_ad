import axios from 'axios';

var TWITCH_PLAYER = 1;
var YOUTUBE_PLAYER = 2;
var playerCount = 1;
function makeid(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function getOffset (el) {
  const box = el.getBoundingClientRect();
  return {
    top: box.top + window.pageYOffset - document.documentElement.clientTop,
    left: box.left + window.pageXOffset - document.documentElement.clientLeft
  };
}

function shortTitle(width, string = '') {
  if (!string.length) {
    return string;
  }
  // default - width (400) divide symbols (55)
  const defaultLengthCoeff = 7.7; // = ~400/55 = 7.3
  
  const strLength = +(width / string.length).toFixed(2);
  if (strLength >= defaultLengthCoeff) {
    return string;
  }
  const words = string.split(' ');
  let currLenght = 0, currStrLengthCoeff = 0, currStr = '';
  for (let i = 0; i < words.length; i++) {
    currLenght = words[i].length + currLenght;
    currLenght++; // add space
    currStrLengthCoeff = +(width / currLenght).toFixed(2);
    currStr = currStr + ' ' + words[i];
    if (currStrLengthCoeff <= defaultLengthCoeff) {
      return currStr + '...';
    }
  }
  return currStr + '...';
}


function getHeight(el) {
  const styles = window.getComputedStyle(el);
  const height = el.offsetHeight;
  const borderTopWidth = parseFloat(styles.borderTopWidth);
  const borderBottomWidth = parseFloat(styles.borderBottomWidth);
  const paddingTop = parseFloat(styles.paddingTop);
  const paddingBottom = parseFloat(styles.paddingBottom);
  return height - borderBottomWidth - borderTopWidth - paddingTop - paddingBottom;
}

function playerControlsStyles(contentId, height) {
  return `
    #uplify__container .player-controls {
      position: absolute;
      top: 0;
      left: 0;
      z-index: 3;
      width: 100%;
      height: ${height}px;
    }
    #uplify__container .player-controls__buttons {
        width: 100%;
        height: 100%;
        background-image: linear-gradient(
            to top,
            rgba(0,0,0,0.4) 0px,
            rgba(0,0,0,0.4) 30px,
            rgba(0,0,0,0) 70px
        );
        transition: opacity 0.2s ease;
    }
    #uplify__container .player-controls__button {
        position: absolute;
        bottom: 5px;
        width: 40px;
        height: 40px;
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        cursor: pointer;
    }
    #uplify__container .player-controls__button::after {
      opacity: 0.01;
      display: block;
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-size: contain;
      background-position: center;
      background-repeat: no-repeat;
    }

    #uplify__container .player-controls__button._play {
        left: 5px;
        background-image: url('../images/ui/play.svg');
        background-size: 25px auto;
    }

    #uplify__container .player-controls__button._play::after {
        background-image: url('../images/ui/pause.svg');
        background-size: 25px auto;
    }

    #uplify__container .player-controls__button._fullscreen {
        right: 60px;
        background-image: url('../images/ui/enter-fullscreen.svg');
        background-size: 25px auto;
    }

    #uplify__container .player-controls__button._fullscreen::after {
      background-image: url('../images/ui/exit-fullscreen.svg');
      background-size: 25px auto;
    }

    #uplify__container .player-controls__button._mute {
        right: 10px;
        background-image: url('../images/ui/sound.svg');
        background-size: 28px auto;
    }

    #uplify__container .player-controls__button._mute::after {
        background-image: url('../images/ui/sound-disabled.svg');
        background-size: 38px auto;
        background-position: center left 6px;
    }

    #${contentId}.vjs-playing .player-controls__button._play {
        background-image: none;
    }

    #${contentId}.vjs-playing .player-controls__button._play::after {
      opacity: 1;
    }

    #${contentId}.vjs-fullscreen .player-controls__button._fullscreen {
            background-image: none;
    }

    #${contentId}.vjs-fullscreen .player-controls__button._fullscreen::after {
      opacity: 1;
    }

    #${contentId}.vjs-muted .player-controls__button._mute {
            background-image: none;
    }
    #${contentId}.vjs-muted .player-controls__button._mute::after {
        opacity: 1;
    } 
    #${contentId}.vjs-has-started.vjs-user-inactive .player-controls__buttons {
      opacity: 0;
    }
  `;
}

/**
 * Handles video player functionality.
 */
var VideoPlayer = function({
    autoplay,
    mode,
    title,
    twitchType,
    width,
    height,
    widthFloated,
    heightFloated
  }, playCb) {
  var videoPlayerId = `videoplayer_${makeid(10)}_${playerCount}`;
  var acContainerId = `adcontainer_${makeid(10)}_${playerCount}`;
  var contentId = `content_${makeid(10)}_${playerCount}`;
  var closeButtonId = `closeButton_${makeid(10)}_${playerCount}`;
  const container = document.getElementById('uplify__container');
  container.setAttribute('data-mode', mode);
  container.innerHTML = `
    <div id="${videoPlayerId}">
      <div id="${closeButtonId}"></div>
      <div id="${contentId}">
      </div>
      <div id="${acContainerId}" style="position: absolute; top: 0px; left: 0px;">
      </div>
    </div>`;
  this.contentPlayer = document.getElementById(contentId);
  this.adContainer = document.getElementById(acContainerId);
  this.videoPlayerContainer_ = document.getElementById(videoPlayerId);
  this.panelTitle = title;

  this.width = width;
  this.height = height;

  this.widthFloated = widthFloated;
  this.heightFloated = heightFloated;
  this.playerType = null; // 1 - twitch, 2 - youtube
  this.firstInit = true;
  this.firstPress = false;
  this.mixedFloated = false;
  this.isOnlineStream = true;
  this.playerStatusPanelClass = '';
  var that = this;

  if (document.body.clientWidth < 700) {
    this.widthFloated = 280;
    this.heightFloated = 160;
  }
  
  if (mode === 'fixed' || mode === 'mixed') {
    const additionalStyles = document.createElement('style');
    const hiddenClass = `hidden_${makeid(10)}_${playerCount}`;
    const fixedClass = `fixed_${makeid(10)}_${playerCount}`;
    const playerStatusPanelClass =  `player-status-panel_${makeid(10)}_${playerCount}`;
    this.playerStatusPanelClass = playerStatusPanelClass;
    const statusPanel = document.createElement('div');
    if (twitchType) {
      statusPanel.setAttribute('style', 'background-image: none; padding: 9px 15px 9px 9px;');
    }
    statusPanel.className = playerStatusPanelClass;
    statusPanel.innerHTML = `<span class="js-video-title">${this.panelTitle}</span>`;
    this.videoPlayerContainer_.prepend(statusPanel);
    additionalStyles.className = 'xd-s';
    additionalStyles.innerHTML = `
    #${videoPlayerId}.${fixedClass} .${playerStatusPanelClass} {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      
    }

    .${playerStatusPanelClass} {
      display: none;
      box-sizing: border-box;
      justify-content: flex-start;
      align-items: center;
      position: relative;
      min-height: 27px;
      width: 100%;
      overflow: hidden;
      padding: 9px 15px 9px 47px;
      text-align: left;
      font-family: Roboto, Helvetica, Arial, sans-serif;
      font-size: 11px;
      line-height: 1;
      font-weight: 500;
      color: #333333;
      background-color: #fff;
      background-image: url('https://player.uplify.app/images/icons/live.svg');
      background-position: left 7px center;
      background-size: auto 16px;
      background-repeat: no-repeat;
  
    }
    .${playerStatusPanelClass} span {
        white-space: nowrap;
    }
    .${playerStatusPanelClass}::after {
      display: block;
      content: "";
      position: absolute;
      z-index: 2;
      top: 0;
      right: 0;
      width: 40px;
      height: 100%;
      background-image: linear-gradient(
          to left,
          rgba(255,255,255,1) 0px,
          rgba(255,255,255,1) 18px,
          rgba(255,255,255,0) 25px,
      );
    }

    #uplify__container {
        display: none;
        margin: 40px 0;
        background-color: #ccc;
    }
   
    #uplify__container.loaded {
        display: block;
    }
    
    #${videoPlayerId} {
        position: relative;
    }
    
    #uplify__container,
    #${videoPlayerId},
    #${contentId} {
        width: ${this.widthFloated}px;
        height: ${+this.heightFloated + 29}px;
    }

    @media (max-width: 700px) {
      #uplify__container,
      #${videoPlayerId},
      #${contentId} {

      }
    }
    
    #${videoPlayerId}.${fixedClass} {
      position: fixed;
      z-index: 40;
      right: 40px;
      bottom: 135px;
      border: 1px solid #999999;
      border-radius: 2px 2px 0 0;
      -webkit-box-shadow: 0px 4px 4px rgba(0,0,0,0.25);
      box-shadow: 0px 4px 4px rgba(0,0,0,0.25);
    }

    @media (max-width: 700px) {
      #${videoPlayerId}.${fixedClass} {
        right: 20px;
        bottom: 45px;
      }
    }
      
    .${hiddenClass} {
        width: 0!important;
        height: 0!important;
        margin: 0!important;
    }
    .${fixedClass} #${closeButtonId} {
        display: block;
    }
    
    @media (max-width: 700px) {
      #${closeButtonId} {
        top: auto!important;
        bottom: calc(100% + 3px);
        left: auto!important;
        right: -1px;
        background-color: #fff;
        border: 1px solid #999999;
      }

      #${closeButtonId}:hover {
        transform: none;
      }
    }

    #${closeButtonId} {
        display: none;
        position: absolute;
        width: 30px;
        height: 30px;
        top: 0px;
        left: 100%;
        cursor: pointer;
    }

    #${closeButtonId}:hover {
      transform: rotate(90deg);
    }
      
    #${closeButtonId}::before,
    #${closeButtonId}::after {
        display: block;
        content: "";
        position: absolute;
        top: calc(50% - 1px);
        left: 5px;
        width: 20px;
        height: 2px;
        background-color: #000;
    }
    
    #${closeButtonId}::before {
        transform: rotate(-45deg);
    }
    
    #${closeButtonId}::after {
        transform: rotate(45deg);
    }}`;
    // ${playerControlsStyles(contentId, this.height + 30)}`;
    document.head.appendChild(additionalStyles);


    if (mode === 'fixed') {
      container.className = `${hiddenClass} loaded`;
      this.videoPlayerContainer_.className = fixedClass;
      document.getElementById(acContainerId).style.top = '29px';
    } else if (mode === 'mixed') {
      container.className = `loaded`;
      initScrollTrack();
    }

    function setNormalPlayer() {
      that.mixedFloated = false;
      that.videoPlayerContainer_.className = '';
      document.getElementById(contentId).getElementsByTagName('iframe')[0].width = that.width;
      document.getElementById(contentId).getElementsByTagName('iframe')[0].height = that.height;
      document.getElementById(acContainerId).style.top = '0px';
      document.getElementById(videoPlayerId).style.height = `${that.height}px`;
      document.getElementById('uplify__container').style.height = `${that.height}px`;
    }

    function setFixedPlayer() {
      that.mixedFloated = true;
      that.videoPlayerContainer_.className = fixedClass;
      document.getElementById(contentId).getElementsByTagName('iframe')[0].width = that.widthFloated;
      document.getElementById(contentId).getElementsByTagName('iframe')[0].height = that.heightFloated;
      document.getElementById(acContainerId).style.top = '29px';
      document.getElementById(videoPlayerId).style.height = `${that.heightFloated}px`;
      document.getElementById(videoPlayerId).style.width = `${that.widthFloated}px`;
      document.getElementById('uplify__container').style.height = `${that.heightFloated}px`;
    }

    function scrollListenner(e) {
      if (that.player && mode === 'mixed') {
        // this.contentPlayer
        try {
          const scrlollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
          if(scrlollTop > getOffset(container).top + getHeight(that.contentPlayer) / 2) {
            setFixedPlayer();
          } else {
            setNormalPlayer();
          }
        } catch (e) { console.warn(e); }
      }
    }
    function initScrollTrack() {
      window.addEventListener('scroll', scrollListenner);
    }
    function resizeListener() {
      if (document.body.clientWidth < 700) {
        that.widthFloated = 280;
        that.heightFloated = 160;
      } else {
        that.widthFloated = widthFloated;
        that.heightFloated = heightFloated;
      }
      try {
        const scrlollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        if(scrlollTop > getOffset(container).top + getHeight(that.contentPlayer) / 2) {
          setFixedPlayer();
        }
      } catch (e) { console.warn(e); }
      
    }
    window.addEventListener('resize', resizeListener);

    document.getElementById(closeButtonId).addEventListener('click', function(e) {
      window.removeEventListener('scroll', scrollListenner);
      if (mode === 'fixed') {
        container.innerHTML = '';
      } else {
        setNormalPlayer();
      }
    });
  } 

  if (window.PLATFORM == 'twitch') {
    var twitchOpts = {
      channel: window.PLATFORM_ID,
    };
    if (this.width && this.height && mode === 'fixed') {
      twitchOpts.width = this.widthFloated;
      twitchOpts.height = this.heightFloated;
    }
    if (this.width && this.height && mode === 'normal') {
      twitchOpts.width = this.width;
      twitchOpts.height = this.heigth;
    }
    if (this.width && this.height && mode === 'mixed') {
      twitchOpts.width = this.widthFloated;
      twitchOpts.height = this.heightFloated;
    }
    // var playerEl = document.getElementsByTagName('iframe').length ? document.getElementsByTagName('iframe')[0] : null;
    if (!twitchOpts.width && !twitchOpts.height) {
      // playerEl.width = '100%';
      // playerEl.height = '100%';
      if (document.getElementById('uplify__container').clientWidth) {
        this.width = document.getElementById('uplify__container').clientWidth;
        this.height = Math.round(this.width / 1.77); // 16:9
      } else {
        this.width = document.getElementById('uplify__container').parentNode.clientWidth;
        this.height = Math.round(this.width / 1.77); // 16:9
      }
    }

    // init player before mixed
    this.player = new Twitch.Player(contentId, twitchOpts);

    if (mode === 'mixed') {
      if (!document.getElementById(videoPlayerId).className) {
        debugger;
        document.getElementById(contentId).getElementsByTagName('iframe')[0].width = this.width;
        document.getElementById(contentId).getElementsByTagName('iframe')[0].height = this.height;
        document.getElementById(videoPlayerId).style.height = `${this.height}px`;
        document.getElementById(videoPlayerId).style.width = `${this.width}px`;
        document.getElementById('uplify__container').style.height = `${this.height}px`;
        // document.getElementById('uplify__container').style.width = `${this.width}px`;
      }
    }

    if (window.TWITCH_TYPE == 'video') {
      twitchOpts.video = window.PLATFORM_ID;
      delete twitchOpts.channel;
    }
    
    this.playerType = TWITCH_PLAYER;
    this.player.setVolume(0.5);

    if (mode !== 'normal') {
      document.querySelector('.js-video-title').innerText = this.panelTitle;
    }

    this.player.addEventListener(Twitch.Player.READY, function() {
      setTimeout(function() {
        if (that.firstInit) {
          if (mode === 'mixed') {
            that.adContainer.style.display = 'none';
            that.player.pause();
          } else if (mode === 'fixed') {
            that.player.setMuted(true);
            that.player.play();
          }
          that.firstInit = false;
        }
  
        that.player.addEventListener(Twitch.Player.PLAY, function() {
          if (!that.firstInit && !that.firstPress) {
            // that.player.pause();
            playCb();
            that.adContainer.style.display = 'block';
            that.firstPress = true;
          }
          // console.log('PLAY');
        });
      }, 3000);
    });

    this.player.addEventListener('online', function () {
      // console.log('ONLINE');
    });
  } else if (window.PLATFORM == 'youtube') {
    // window.removeEventListener('scroll', scrollListenner);
    if (document.querySelectorAll('.xd-s').length) {
      for (let i = 0; i < document.querySelectorAll('.xd-s').length; i++) {
        document.querySelectorAll('.xd-s')[i].remove();
      }
    }
    for (let i = 0; i < document.querySelectorAll('script').length; i++) {
      if (document.querySelectorAll('script')[i].src === 'https://www.youtube.com/iframe_api') {
        document.querySelectorAll('script')[i].remove();
      }
    }
    
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    window.onYouTubeIframeAPIReady = function() {
       console.log("YouTube API Ready");
       var youtubeBlock = document.createElement('div');
       youtubeBlock.setAttribute('id', 'youtubeContainer');
       document.getElementById(contentId).append(youtubeBlock);
       that.player = new YT.Player('youtubeContainer', { 
           videoId: window.PLATFORM_ID, //'9gb8z4R39LE',
           playerVars: {
               controls: 1,
               autoplay: 0,
               disablekb: 1,
               enablejsapi: 1,
               iv_load_policy: 3,
               // modestbranding: 1,
               showinfo: 1
           },
           // try disable for response video
          // height: window.PLAYER_HEIGHT,
          // width: window.PLAYER_WIDTH,
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
       }); 
       that.playerType = YOUTUBE_PLAYER;
      //  ytLoaded = true;
    };

    // 4. The API will call this function when the video player is ready.
    window.onPlayerReady = function (event) {
      // event.target.playVideo();
    };

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;
    window.onPlayerStateChange = function (event) {
      // if (event.data == YT.PlayerState.PLAYING && !done) {
      //   setTimeout(stopVideo, 6000);
      //   done = true;
      // }
      if (event.data == YT.PlayerState.PLAYING) {
        if (that.firstInit) {
          playCb();
          that.adContainer.style.display = 'block';
          that.firstPress = true;
          that.firstInit = false;
        }
      }
      // console.log('STATE', event.data);
    };
    window.stopVideo = function() {
      player.stopVideo();
    };
  }
  playerCount++;
};


VideoPlayer.prototype.setPanelTitle = function(title) {
  this.panelTitle = shortTitle(this.widthFloated, title);
  if (this.videoPlayerContainer_.getElementsByClassName('js-video-title').length) {
    this.videoPlayerContainer_.getElementsByClassName('js-video-title')[0].innerText = this.panelTitle;
  }
  if (!this.isOnlineStream) {
    this.videoPlayerContainer_.getElementsByClassName(this.playerStatusPanelClass)[0].style['background-image'] = 'none';
  }
};

VideoPlayer.prototype.hideAd = function() {
  this.adContainer.innerHTML = '';
  if (this.isMuted()) {
    this.setVolume(0);
  } else {
    this.setVolume(0.7);
  }
  
  // this.play();
  // this.adContainer.style.display = 'none';
  // console.log('ADS END');
};

VideoPlayer.prototype.preloadContent = function(contentLoadedAction) {
  // If this is the initial user action on iOS or Android device,
  // simulate playback to enable the video element for later program-triggered
  // playback.
  if (this.isMobilePlatform()) {
    this.contentPlayer.addEventListener(
        'loadedmetadata',
        contentLoadedAction,
        false);
    this.contentPlayer.load();
  } else {
    contentLoadedAction();
  }
};

VideoPlayer.prototype.play = function() {
  if (this.playerType === TWITCH_PLAYER) {
    this.player.play();
  } else if (this.playerType === YOUTUBE_PLAYER) {
    this.player.playVideo();
  } else {
    throw new Error('Wrong player type')
  }
};

VideoPlayer.prototype.pause = function() {
  if (this.playerType === TWITCH_PLAYER) {
    this.player.pause();
  } else if (this.playerType === YOUTUBE_PLAYER) {
    this.player.pauseVideo();
  } else {
    throw new Error('Wrong player type')
  }
};

VideoPlayer.prototype.isPaused = function() {
  if (this.playerType === TWITCH_PLAYER) {
    return this.player.isPaused();
  } else if (this.playerType === YOUTUBE_PLAYER) {
    // -1 – unstarted
    // 0 – ended
    // 1 – playing
    // 2 – paused
    // 3 – buffering
    // 5 – video cued
    return this.player.getPlayerState() == 2;
  } else {
    throw new Error('Wrong player type');
  }
};

VideoPlayer.prototype.isMuted = function() {
  if (this.playerType === TWITCH_PLAYER) {
    return this.player.getMuted();
  } else if (this.playerType === YOUTUBE_PLAYER) {
    return this.player.isMuted();
  } else {
    throw new Error('Wrong player type');
  }
};


VideoPlayer.prototype.isMobilePlatform = function() {
  return this.contentPlayer.paused &&
      (navigator.userAgent.match(/(iPod|iPhone|iPad)/) ||
       navigator.userAgent.toLowerCase().indexOf('android') > -1);
};

VideoPlayer.prototype.resize = function(
    position, top, left, width, height) {
  this.videoPlayerContainer_.style.position = position;
  this.videoPlayerContainer_.style.top = top + 'px';
  this.videoPlayerContainer_.style.left = left + 'px';
  this.videoPlayerContainer_.style.width = width + 'px';
  this.videoPlayerContainer_.style.height = height + 'px';
  this.contentPlayer.style.width = width + 'px';
  this.contentPlayer.style.height = height + 'px';
};

VideoPlayer.prototype.registerVideoEndedCallback = function(callback) {
  this.contentPlayer.addEventListener(
      'ended',
      callback,
      false);
};

VideoPlayer.prototype.setVolume = function(volume) {
  if (this.playerType === TWITCH_PLAYER) {
    return this.player.setVolume(volume);
  } else if (this.playerType === YOUTUBE_PLAYER) {
    return this.player.setVolume(volume * 10);
  } else {
    throw new Error('Wrong player type');
  }
};

/**
 * Shows how to use the IMA SDK to request and display ads.
 */
var Ads = function(application, videoPlayer, showMode) {
  this.application_ = application;
  this.videoPlayer_ = videoPlayer;
  this.showMode = showMode;
  this.contentCompleteCalled_ = false;
  this.adDisplayContainer_ =
      new google.ima.AdDisplayContainer(this.videoPlayer_.adContainer, this.videoPlayer_.contentPlayer);
  this.adsLoader_ = new google.ima.AdsLoader(this.adDisplayContainer_);
  var mode = google.ima.ImaSdkSettings.VpaidMode.INSECURE;
  this.adsLoader_.getSettings().setVpaidMode(mode);
  this.adsManager_ = null;

  this.adsLoader_.addEventListener(
      google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
      this.onAdsManagerLoaded_,
      false,
      this);
  this.adsLoader_.addEventListener(
      google.ima.AdErrorEvent.Type.AD_ERROR,
      this.onAdError_,
      false,
      this);
};

// On iOS and Android devices, video playback must begin in a user action.
// AdDisplayContainer provides a initialize() API to be called at appropriate
// time.
// This should be called when the user clicks or taps.
Ads.prototype.initialUserAction = function() {
  this.adDisplayContainer_.initialize();
};

Ads.prototype.requestXml = function(adXML) {
  // Mobile calls requestXml when video source changes.
  // The VPAID ad will change the video source.
  if (this.adsManager_ != null) {
    return;
  }
  var adsRequest = new google.ima.AdsRequest();
  adsRequest.adTagUrl = '';  // No url, using xml instead

  adsRequest.adsResponse = adXML;
  if (this.showMode === 'fixed' || (this.showMode === 'mixed' && this.videoPlayer_.mixedFloated === true)) {
    adsRequest.linearAdSlotWidth = this.videoPlayer_.widthFloated;
    adsRequest.linearAdSlotHeight = this.videoPlayer_.heightFloated;
    adsRequest.nonLinearAdSlotWidth = this.videoPlayer_.widthFloated;
    adsRequest.nonLinearAdSlotHeight = this.videoPlayer_.heightFloated;
  } else {
    adsRequest.linearAdSlotWidth = this.videoPlayer_.width;
    adsRequest.linearAdSlotHeight = this.videoPlayer_.height;
    adsRequest.nonLinearAdSlotWidth = this.videoPlayer_.width;
    adsRequest.nonLinearAdSlotHeight = this.videoPlayer_.height;
  }
  this.adsLoader_.requestAds(adsRequest);
};

Ads.prototype.pause = function() {
  if (this.adsManager_) {
    this.adsManager_.pause();
  }
};

Ads.prototype.resume = function() {
  if (this.adsManager_) {
    this.adsManager_.resume();
  }
};

Ads.prototype.resize = function(width, height) {
  if (this.adsManager_) {
    this.adsManager_.resize(width, height, google.ima.ViewMode.FULLSCREEN);
  }
};

Ads.prototype.contentEnded = function() {
  this.contentCompleteCalled_ = true;
  this.adsLoader_.contentComplete();
  this.videoPlayer_.hideAd();
};

Ads.prototype.onAdsManagerLoaded_ = function(adsManagerLoadedEvent) {
  this.application_.log('Ads loaded.');
  this.adsManager_ = adsManagerLoadedEvent.getAdsManager(this.videoPlayer_.contentPlayer);
  if (this.videoPlayer_.isMuted()) {
    this.adsManager_.setVolume(0);
  } else {
    this.adsManager_.setVolume(0.7);
  }
  this.processAdsManager_(this.adsManager_);
};

Ads.prototype.clear = function() {
  this.adsLoader_.removeEventListener(
      google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
      this.onAdsManagerLoaded_,
      false,
      this);
  this.adsLoader_.removeEventListener(
      google.ima.AdErrorEvent.Type.AD_ERROR,
      this.onAdError_,
      false,
      this);
  if (this.adsManager_) {
    this.adsManager_.removeEventListener(
        google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED,
        this.onContentPauseRequested_,
        false,
        this);
    this.adsManager_.removeEventListener(
        google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED,
        this.onContentResumeRequested_,
        false,
        this);
    this.adsManager_.removeEventListener(
        google.ima.AdErrorEvent.Type.AD_ERROR,
        this.onAdError_,
        false,
        this);
    var events = [google.ima.AdEvent.Type.ALL_ADS_COMPLETED,
          google.ima.AdEvent.Type.CLICK,
          google.ima.AdEvent.Type.COMPLETE,
          google.ima.AdEvent.Type.FIRST_QUARTILE,
          google.ima.AdEvent.Type.LOADED,
          google.ima.AdEvent.Type.MIDPOINT,
          google.ima.AdEvent.Type.PAUSED,
          google.ima.AdEvent.Type.STARTED,
          google.ima.AdEvent.Type.THIRD_QUARTILE];
    for (var index in events) {
      this.adsManager_.removeEventListener(
        events[index],
        this.onAdEvent_,
        false,
        this);
    }
  }
}

Ads.prototype.processAdsManager_ = function(adsManager) {
  // Attach the pause/resume events.
  adsManager.addEventListener(
      google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED,
      this.onContentPauseRequested_,
      false,
      this);
  adsManager.addEventListener(
      google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED,
      this.onContentResumeRequested_,
      false,
      this);
  // Handle errors.
  adsManager.addEventListener(
      google.ima.AdErrorEvent.Type.AD_ERROR,
      this.onAdError_,
      false,
      this);
  var events = [google.ima.AdEvent.Type.ALL_ADS_COMPLETED,
                google.ima.AdEvent.Type.CLICK,
                google.ima.AdEvent.Type.COMPLETE,
                google.ima.AdEvent.Type.FIRST_QUARTILE,
                google.ima.AdEvent.Type.LOADED,
                google.ima.AdEvent.Type.MIDPOINT,
                google.ima.AdEvent.Type.PAUSED,
                google.ima.AdEvent.Type.STARTED,
                google.ima.AdEvent.Type.THIRD_QUARTILE];
  for (var index in events) {
    adsManager.addEventListener(
        events[index],
        this.onAdEvent_,
        false,
        this);
  }

  var initWidth, initHeight;
  if (this.application_.fullscreen) {
    initWidth = this.application_.fullscreenWidth;
    initHeight = this.application_.fullscreenHeight;
  } else {
    if (this.showMode === 'fixed' || (this.showMode === 'mixed' && this.videoPlayer_.mixedFloated === true)) {
      initWidth = this.videoPlayer_.widthFloated;
      initHeight = this.videoPlayer_.heightFloated;
    } else {
      initWidth = this.videoPlayer_.width;
      initHeight = this.videoPlayer_.height;
    }
  }
  adsManager.init(
    initWidth,
    initHeight,
    google.ima.ViewMode.NORMAL);

  adsManager.start();
};

Ads.prototype.onContentPauseRequested_ = function(adErrorEvent) {
  // DISABLE PAUSE HERE - MOVE TO onAdEventStarted
  // this.application_.pauseForAd();
  // console.log('VIDEO MUST BE PAUSED')
};

Ads.prototype.onContentResumeRequested_ = function(adErrorEvent) {
  // Without this check the video starts over from the beginning on a
  // post-roll's CONTENT_RESUME_REQUESTED
  if (!this.contentCompleteCalled_) {
    this.application_.resumeAfterAd();
  }
};

Ads.prototype.onAdEvent_ = function(adEvent) {
  this.application_.log('Ad event: ' + adEvent.type);

  if (adEvent.type == google.ima.AdEvent.Type.STARTED) {
    this.application_.pauseForAd();
  }
  if (adEvent.type == google.ima.AdEvent.Type.CLICK) {
    this.application_.adClicked();
  }
  if (adEvent.type == google.ima.AdEvent.Type.COMPLETE || 
      adEvent.type == google.ima.AdEvent.Type.ALL_ADS_COMPLETED) {
    this.videoPlayer_.hideAd();
  }
};

Ads.prototype.onAdError_ = function(adErrorEvent) {
  this.application_.log('Ad error: ' + adErrorEvent.getError().toString());
  if (this.adsManager_) {
    this.adsManager_.destroy();
  }
  this.application_.resumeAfterAd();
  this.videoPlayer_.hideAd();
};


/**
 * Handles user interaction and creates the player and ads controllers.
 */
var Application = function({
  platform,
  twitchType,
  platformId,
  sectionId,
  width,
  height,
  widthFloated = 400,
  heightFloated = 225,
  interval,
  autoplay = false,
  mode = 'normal',
  title = document.title
}) {
  if (!platform || ['twitch', 'youtube'].indexOf(platform) === -1) {
    throw new Error('Unknown platform');
  }
  if (platform === 'twtich' && !twtichType) {
    throw new Error('Wrong twitch type');
  }
  if (!platformId) {
    throw new Error('Wrong video indentificator');
  }
  if (!sectionId) {
    throw new Error('Wrong section id from between');
  }
  // if (!w || !h) {
  //   throw new Error('Width or height of a player is empty');
  // }
  window.PLATFORM = platform;
  window.TWITCH_TYPE = twitchType;
  window.PLATFORM_ID = platformId;
  window.SECTION_ID = sectionId;
  window.PLAYER_WIDTH = width;
  window.PLAYER_HEIGHT = height;
  window.INTERVAL = +interval > 60 ? interval : 60;

  // this.playButton_ = document.getElementById('playpause');

  // this.loadAdButton = document.getElementById('loadad');
  // this.loadAdButton.addEventListener('click', () => {
  //   this.makeRequest_('https://match.ads.betweendigital.com/vpaid_prod?s='+
  //   window.SECTION_ID + '&maxd=300&mind=1&w=' + window.PLAYER_WIDTH + '&h=' + window.PLAYER_HEIGHT)
  //   .then(res => {
  //     this.setXml_(res.data);
  //   })
  //   .catch(err => {
  //     this.log(err);
  //   });
  // }, false);

  // this.playButton_.addEventListener(
  //     'click',
  //     this.bind_(this, this.onClick_),
  //     false);

  // this.fullscreenButton_ = document.getElementById('fullscreen');
  // this.fullscreenButton_.addEventListener(
  //     'click',
  //     this.bind_(this, this.onFullscreenClick_),
  //     false);

  this.fullscreenWidth = null;
  this.fullscreenHeight = null;

  // var fullScreenEvents = [
  //     'fullscreenchange',
  //     'mozfullscreenchange',
  //     'webkitfullscreenchange'];
  // for (key in fullScreenEvents) {
  //   document.addEventListener(
  //       fullScreenEvents[key],
  //       this.bind_(this, this.onFullscreenChange_),
  //       false);
  // }

  this.playing_ = false;
  this.adsActive_ = false;
  this.adsDone_ = false;
  this.fullscreen = false;
  this.lastAdShow = null;
  var that = this;

  this.videoPlayer_ = new VideoPlayer({
    autoplay,
    mode,
    title,
    twitchType,
    width,
    height,
    widthFloated,
    heightFloated,
  }, function() {
    that.onClick_();
    that.lastAdShow = Math.round( new Date().getTime() / 1000 );
  });
  axios(`https://api.uplify.app/v1/twitch/checkOnline?username=${platformId}`)
    .then(r => {
      if (r.data && r.data.stream && r.data.stream.channel.status) {
        that.videoPlayer_.setPanelTitle(r.data.stream.channel.status);
      } else if (r.data && r.data.video && r.data.video.title) {
        that.videoPlayer_.isOnlineStream = false;
        that.videoPlayer_.setPanelTitle(r.data.video.title);
      } else {
        that.videoPlayer_.isOnlineStream = false;
        that.videoPlayer_.setPanelTitle(document.title);
      }
    })
    .catch(e => {
      that.videoPlayer_.isOnlineStream = false;
      that.videoPlayer_.setPanelTitle(document.title);
      console.warn(e, 'Cannot get stream data')
    });
  this.ads_ = new Ads(this, this.videoPlayer_, mode);
  this.adXml_ = '';

  this.videoPlayer_.registerVideoEndedCallback(
      this.bind_(this, this.onContentEnded_));
  this.httpRequest_ = null;
  let ref = window.location.host;
  if (window.location.host.indexOf('esforce') !== -1) {
    ref = 'cybersport.ru';
  }

  var url = 'https://match.ads.betweendigital.com/vpaid_prod?s=' + window.SECTION_ID + '&ref=' + ref + '&client_auction=1';
  // only section
  // !window.PLAYER_WIDTH && !window.PLAYER_HEIGHT ? 
  //   'https://match.ads.betweendigital.com/vpaid_prod?s=' + window.SECTION_ID + '&maxd=300&mind=1&ref=' + window.location.host :
  //   'https://match.ads.betweendigital.com/vpaid_prod?s=' + window.SECTION_ID + '&maxd=300&mind=1&w=' + window.PLAYER_WIDTH + '&h=' + window.PLAYER_HEIGHT + '&ref=' + window.location.host;
  

  this.makeRequest_(url)
    .then(function (res) {
      that.setXml_(res.data);
    })
    .catch(function (err) {
      that.log(err);
    });
  this.isFirstAdShow = false;
 
  setInterval(function() {
    var now = Math.round( new Date().getTime() / 1000 );
    var interval = window.INTERVAL * 1;
    if (that.lastAdShow && (now - interval) > that.lastAdShow && !that.videoPlayer_.isPaused()) {

      try {
        that.ads_.clear();
        that.ads_ = new Ads(that, that.videoPlayer_, mode);
        that.adsActive_ = false;
        that.adsDone_ = false;
        that.playing_ = false;
        that.onClick_();
      } catch (e) {console.warn('Cannot run Ad', e);}
      that.lastAdShow = Math.round( new Date().getTime() / 1000 );
    }
  }, 10 * 1000)
};

Application.prototype.log = function(message) {
  console.log(message);
  // this.console_.innerHTML = this.console_.innerHTML + '<br/>' + message;
};

Application.prototype.resumeAfterAd = function() {
  this.videoPlayer_.play();
  this.adsActive_ = false;
  this.updateChrome_();
};

/**
 * Begin show ad
 */
Application.prototype.pauseForAd = function() {
  this.videoPlayer_.adContainer.style.display = 'block';
  this.adsActive_ = true;
  this.playing_ = true;
  this.videoPlayer_.setVolume(0);
  // DISABLINIG PAUSE STREAM
  // this.videoPlayer_.pause();
  // console.log('VIDOEPLAYER PAUSED');
  this.updateChrome_();
};

Application.prototype.adClicked = function() {
  this.updateChrome_();
};

Application.prototype.bind_ = function(thisObj, fn) {
  return function() {
    fn.apply(thisObj, arguments);
  };
};

Application.prototype.makeRequest_ = function(url) {
  return axios.get(url);
  if (window.XMLHttpRequest) {
    this.httpRequest_ = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    try {
      this.httpRequest_ = new ActiveXObject("Msxml2.XMLHTTP");
    } 
    catch (e) {
      this.httpRequest_ = new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
  this.httpRequest_.onreadystatechange = this.bind_(this, this.setXml_);
  this.httpRequest_.open('GET', url);
  this.httpRequest_.send();
};

Application.prototype.setXml_ = function(data) {
  this.xmlBox_ = data;
  // this.xmlBox_ = this.httpRequest_.responseText;
};

Application.prototype.onClick_ = function() {
  if (!this.adsDone_) {
    this.log('Begin ad show...');
    if (this.xmlBox_ == '') {
      this.log("Error: please fill in xml");
      return;
    } else {
      this.adXml_ = this.xmlBox_;
    }
    // The user clicked/tapped - inform the ads controller that this code
    // is being run in a user action thread.
    this.ads_.initialUserAction();
    // At the same time, initialize the content player as well.
    // When content is loaded, we'll issue the ad request to prevent it
    // from interfering with the initialization. See
    // https://developers.google.com/interactive-media-ads/docs/sdks/html5/v3/ads#iosvideo
    // for more information.
    this.videoPlayer_.preloadContent(this.bind_(this, this.loadAds_));
    this.adsDone_ = true;
    return;
  }

  if (this.adsActive_) {
    if (this.playing_) {
      this.ads_.pause();
    } else {
      this.ads_.resume();
    }
  } else {
    if (this.playing_) {
      this.videoPlayer_.pause();
    } else {
      this.videoPlayer_.play();
    }
  }

  this.playing_ = !this.playing_;

  this.updateChrome_();
};

Application.prototype.onFullscreenClick_ = function() {
  if (this.fullscreen) {
    // The video is currently in fullscreen mode
    var cancelFullscreen = document.exitFullScreen ||
        document.webkitCancelFullScreen ||
        document.mozCancelFullScreen;
    if (cancelFullscreen) {
      cancelFullscreen.call(document);
    } else {
      this.onFullscreenChange_();
    }
  } else {
    // Try to enter fullscreen mode in the browser
    var requestFullscreen = document.documentElement.requestFullScreen ||
        document.documentElement.webkitRequestFullScreen ||
        document.documentElement.mozRequestFullScreen;
    if (requestFullscreen) {
      this.fullscreenWidth = window.screen.width;
      this.fullscreenHeight = window.screen.height;
      requestFullscreen.call(document.documentElement);
    } else {
      this.fullscreenWidth = window.innerWidth;
      this.fullscreenHeight = window.innerHeight;
      this.onFullscreenChange_();
    }
  }
  requestFullscreen.call(document.documentElement);
};

Application.prototype.updateChrome_ = function() {
  if (this.playing_) {
    // this.playButton_.textContent = 'II';
  } else {
    // Unicode play symbol.
    // this.playButton_.textContent = String.fromCharCode(9654);
  }
};

Application.prototype.loadAds_ = function() {
  this.ads_.requestXml(this.adXml_);
};

Application.prototype.onFullscreenChange_ = function() {
  if (this.fullscreen) {
    // The user just exited fullscreen
    // Resize the ad container
    this.ads_.resize(
        this.videoPlayer_.width,
        this.videoPlayer_.height);
    // Return the video to its original size and position
    this.videoPlayer_.resize(
        'relative',
        '',
        '',
        this.videoPlayer_.width,
        this.videoPlayer_.height);
    this.fullscreen = false;
  } else {
    // The fullscreen button was just clicked
    // Resize the ad container
    var width = this.fullscreenWidth;
    var height = this.fullscreenHeight;
    this.makeAdsFullscreen_();
    // Make the video take up the entire screen
    this.videoPlayer_.resize('absolute', 0, 0, width, height);
    this.fullscreen = true;
  }
};

Application.prototype.makeAdsFullscreen_ = function() {
  this.ads_.resize(
      this.fullscreenWidth,
      this.fullscreenHeight);
};

Application.prototype.onContentEnded_ = function() {
  this.ads_.contentEnded();
};

window.UPApp = Application;

