var express = require('express');
var router = express.Router();
const path = require('path');
const rp = require('request-promise');
const sqlite3 = require('sqlite3').verbose();

/* GET home page. */
router.get('/p', function(req, res, next) {
  if (!req.query.id) {
    throw new Error('Missing param - id');
  }
  if (!req.query.platform) {
    throw new Error('Missing param - platform');
  }
  // res.sendFile(path.join(__dirname, './../public/player/build/index.html'));
  res.render('index', { 
    title: 'Express',
    platform: req.query.platform || 'Unknown platform',
    id: req.query.id,
    twitch_type: req.query.twitch_type || '',
    section_id: req.query.section_id,
    interval: req.query.interval || 60,
    width: req.query.w || '',
    height: req.query.h || '',
    autoplay: req.query.autoplay || '',
  });
});

router.get('/test', function(req, res, next) {
  // res.sendFile(path.join(__dirname, './../public/player/build/index.html'));
  res.render('test', { 
    platform: 'twitch',
    platformId: req.query.platformId,
    twitchType: req.query.twitchType || '',
    sectionId: req.query.sectionId,
    interval: req.query.interval || 60,
    width: req.query.width || '',
    height: req.query.height || '',
    widthFloated: req.query.widthFloated || '',
    heightFloated: req.query.heightFloated || '',
    mode: req.query.mode || '',
    title: req.query.title || '',
  });
});

router.get('/test-ima', function(req, res, next) {
  // res.sendFile(path.join(__dirname, './../public/player/build/index.html'));
  res.render('test-ima', { 

  });
});


router.get('/v', function(req, res, next) {
  if (!req.query.id) {
    throw new Error('Missing param - id');
  }
  if (!req.query.platform) {
    throw new Error('Missing param - platform');
  }
  // res.sendFile(path.join(__dirname, './../public/player/build/index.html'));
  res.render('v', { 
    title: 'Express',
    platform: req.query.platform || 'Unknown platform',
    id: req.query.id,
    twitch_type: req.query.twitch_type || '',
    section_id: req.query.section_id,
    interval: req.query.interval || 60,
    width: req.query.w || '',
    height: req.query.h || '',
  });
});

router.get('/demo-test', function(req, res, next) {
  res.render('demo', {});
});


router.get('/cq', function(req, res, next) {
  res.render('cq', {});
});

router.get('/cybersport', function(req, res, next) {
  res.render('cybersport', {});
});

router.get('/rbkgames', function(req, res, next) {
  res.render('rbkgames', {});
});

router.get('/legalbet', function(req, res, next) {
  res.render('legalbet', {});
});

router.get('/cybersports', function(req, res, next) {
  res.render('cybersports', {});
});

router.get('/kanobu', function(req, res, next) {
  res.render('kanobu', {});
});

router.get('/', function(req, res, next) {
  res.render('contacts', { 

  });
});

router.get('/stats', function(req, res, next) {
  if ('player.uplify.app' === req.get('host')) {
    return res.redirect('https://stats-player.uplify.app/stats');
  }
  res.sendFile(path.join(__dirname + '/../public/stats/build/index.html'));
});
router.get('/stats-new', function(req, res, next) {
  res.sendFile(path.join(__dirname + '/../public/Vibe/build/index.html'));
});
router.get('/stats-new/*', function(req, res, next) {
  res.sendFile(path.join(__dirname + '/../public/Vibe/build/index.html'));
});

router.post('/stats/login', function(req, res, next) {
  let db = new sqlite3.Database(path.join(__dirname, '/../stats.sqlite'), (err) => {
    if (err) {
      console.error(err.message);
      return res.status(422).json({
        message: 'Cant connect to db'
      });
    }
    console.log('Connected to the database.');
  });

  let sql = `SELECT * FROM users WHERE name  = ? AND password = ?`;   
  let name = req.body.userName, password = req.body.password;
  console.log('name', name, 'password', password);
  // first row only
  db.get(sql, [name, password], (err, row) => {
    if (err) {
      console.error(err.message);
      return res.status(422).json({
        message: 'Some error'
      });
    }
    if (!row) {
      return res.status(422).json({
        message: 'User not found'
      });
    }
    delete row.password;
    return res.json({
      user: row
    });
  });
  db.close();
});

router.get('/stats/data', async function(req, res, next) {
  console.log('PARAMS', req.query.siteId, req.query.sectionId);
    const opts = {
      headers: {
          "Authorization": 'Bearer 1SdXBx',
      },
      method: "GET",
      uri: `https://cp.betweendigital.com/ru/api/v2/statistic/sites/${req.query.siteId}/sections/${req.query.sectionId}?sdate=${req.query.dateFrom}&edate=${req.query.dateTo}`,
      json: true, // Automatically stringifies the body to JSON
  };
  try {
    let response = await rp(opts);
    const total = {
      payout_payable: 0,
      cpm_payable: 0,
      imps_noads: 0,
      imps_total: 0,
      clicks_total: 0,
      noads: 0
    };
    if (response.data) {
      for (let i = 0; i < Object.keys(response.data).length; i++) {
        const key = Object.keys(response.data)[i];
        const statRow = response.data[key];
        response.data[key].payout_payable = (
          response.data[key].payout_payable - (response.data[key].payout_payable * 0.3)
        ).toFixed(2);
        response.data[key].cpm_payable = Math.floor(
          response.data[key].cpm_payable - (response.data[key].cpm_payable * 0.3)
        );
        response.data[key].noads = response.data[key].imps_noads - response.data[key].imps_rtb;

        total.payout_payable += +response.data[key].payout_payable;
        total.cpm_payable += response.data[key].cpm_payable;
        total.imps_noads += response.data[key].imps_noads;
        total.imps_total += response.data[key].imps_total;
        total.clicks_total += response.data[key].clicks_total;
        total.noads += response.data[key].noads;
      }
      const totalDateRows = Object.keys(response.data).length;
      total.cpm_payable = totalDateRows > 0 ? Math.floor(total.cpm_payable / totalDateRows) : 0;
      total.payout_payable = total.payout_payable.toFixed(2);
      response.data.total = total;
      return res.json(response.data);
    } else {
      return res.status(422).json({
        message: 'nothing to show'
      });
    }
  } catch (e) {
    return res.status(422).json({
      message: e.message,

    });
  }

});

router.get('/stats/*', function(req, res, next) {
  if ('player.uplify.app' === req.get('host')) {
    return res.redirect('https://stats-player.uplify.app/stats');
  }
  res.sendFile(path.join(__dirname + '/../public/stats/build/index.html'));
});


module.exports = router;